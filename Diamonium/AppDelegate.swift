//
//  AppDelegate.swift
//  Diamonium
//


import UIKit
import IQKeyboardManagerSwift
import Fabric
//import Crashlytics
import Firebase
import UserNotifications
import UserNotificationsUI
import AVFoundation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var i:Any? = nil

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
       // Fabric.with([Crashlytics.self])
        
        integrateFirebase(app: application)
            //   print(i!)
        IQKeyboardManager.shared.enable = true
        setInitialController()
        handleLaunchUserActivity(launchOptions)
        requestCamera()
        
        return true
    }
    
    func handleLaunchUserActivity(_ launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        if let notificationData = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [String: Any] {
            handleNotificationData(userInfo: notificationData)
        }
    }
    
        func requestCamera() {
            switch AVCaptureDevice.authorizationStatus(for: .video) {
            case .denied:
                print("Denied, request permission from settings")
                presentCameraSettings()
            case .restricted:
                print("Restricted, device owner must approve")
            case .authorized:
                print("Authorized, proceed")
            case .notDetermined:
                AVCaptureDevice.requestAccess(for: .video) { success in
                    if success {
                        print("Permission granted, proceed")
                    } else {
                        print("Permission denied")
                    }
                }
            }
        }
        
        func presentCameraSettings() {
            let alertController = UIAlertController(title: "Error",
                                                    message: "Camera access is denied",
                                                    preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Cancel", style: .default))
            alertController.addAction(UIAlertAction(title: "Settings", style: .cancel) { _ in
                if let url = URL(string: UIApplication.openSettingsURLString) {
                    UIApplication.shared.open(url, options: [:], completionHandler: { _ in
                        // Handle
                    })
                }
            })
            
            self.window?.rootViewController?.present(alertController, animated: true)
        }
    
    @objc func handleNotificationData(userInfo: [AnyHashable: Any]) {
        if let info = userInfo["gcm.notification.data"] as? String {
            if let data = convertStringToDictionary(text: info) {
                let id  = data["trxid"] as? String
                let blockId = data["block_id"] as? Int ?? 0
                let dateTime = data["datetime"] as? Int
                let name = data["name"] as? String
                let amount = "\(data["amount"]!)"
                let authorizer = data["authorizer"] as? String
                let toName = data["to_name"] as? String
                let type = data["type"] as? Int
                let assetId = data["assetId"] as? String
                let memoText = ""//data[""] as? Int ?? 0
                
                DBManager.sharedInstance.addTransaction(trxList: [Transaction(id: id, name: name, dateTime: dateTime, amount: amount ?? "0", type: type ?? K_RECEIVE, blockId: blockId,memoText: memoText,to_name:toName, authorizer: authorizer, assetId: assetId)], accNumber: toName)
                NotificationCenter.default.post(name: Notification.Name("updateTransactionHistory"), object: nil)
                let userToken = DBManager.sharedInstance.getUserFromUsername(toName!).token
                self.getBalance(userToken, toName: toName)
            }
        }
    }
    
    func getBalance(_ userToken: String?, toName: String?) {
        DispatchQueue.global(qos: .background).sync {
            SessionManager.shared.apiCallToGetBalance(token: userToken) { (balance, tokenBalance) in
                DispatchQueue.main.async {
                    DBManager.sharedInstance.updateUserData(name: nil, email: nil, imagePath: nil, currency: nil, bteBalance: tokenBalance, currencyBalance: balance, accNumber: toName)
                    NotificationCenter.default.post(name: NSNotification.Name("updateBalance"), object: userToken)
                }
            }
        }
    }
    
    func convertStringToDictionary(text: String) -> [String: Any]? {
        if let data = text.replacingOccurrences(of: "\n", with: "").data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        let resignAppTimestamp = Date().timeIntervalSince1970
        UserDefaults.standard.set(resignAppTimestamp, forKey: K_RESIGN_APP_TIMESTAMP)
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        let resignAppTimestamp = UserDefaults.standard.value(forKey: K_RESIGN_APP_TIMESTAMP) as! TimeInterval
        let currentTimestamp = Date().timeIntervalSince1970
        
        if let lockTimer = Singleton.shared.autoLockTimer, (currentTimestamp - resignAppTimestamp >= lockTimer) && Singleton.shared.enableAuthentication {
            passCodeScreen == "Appdelegate"
            Singleton.shared.authenticationWithTouchID(cancel: false) {
            }
        }
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

extension AppDelegate {
    func    setInitialController() {
        guard let token = UserDefaults.standard.string(forKey: K_TOKEN) else {
            Router.launchSplash()
            Router.apiCallToGetSocial()
            return
        }
        if Singleton.shared.enableAuthentication {
            passCodeScreen == "Appdelegate"
            Singleton.shared.authenticationWithTouchID(cancel: false) {
                DispatchQueue.main.async {
                    Router.launchTabVC()
                    Router.apiCallToGetSocial()
                }
            }
        } else {
            Router.launchTabVC()
        }
    }
    
    func integrateFirebase(app: UIApplication) {
        FirebaseApp.configure()
        
        UNUserNotificationCenter.current().delegate = self
        Messaging.messaging().delegate = self
        
        let authOptions: UNAuthorizationOptions = [.alert, .sound, .badge]
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (_, _) in
        }
        app.registerForRemoteNotifications()
    }
}
extension AppDelegate: UNUserNotificationCenterDelegate {
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound, .badge])
    }
}
extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("fcm token: \(fcmToken)")
        UserDefaults.standard.set(fcmToken, forKey: K_FIREBASE_TOKEN)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        completionHandler(.newData)
        UserDefaults.standard.set(userInfo, forKey: "notificationInfo")
       // performSelector(inBackground: #selector(handleNotificationData(userInfo:)), with: userInfo)
        handleNotificationData(userInfo: userInfo)
    }
}
