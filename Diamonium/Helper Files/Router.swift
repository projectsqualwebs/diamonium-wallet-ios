//
//  Router.swift
//  Diamonium


import UIKit
import Foundation

class Router {
    
    static func launchSplash() {
        let splachScreen = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SplashScreenViewController") as! SplashScreenViewController
        getRootViewController().pushViewController(splachScreen, animated: false)
    }
    
    static func launchTabVC() {
        let tabController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabsViewController") as! TabsViewController
        getRootViewController().pushViewController(tabController, animated: false)
    }
    
    static func launchPasscodeVC() {
        let passcodeController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EnterPasscodeViewController") as! EnterPasscodeViewController
        getRootViewController().pushViewController(passcodeController, animated: false)
    }
    
    static func getRootViewController() -> UINavigationController {
        let navController = UINavigationController()
        self.getWindow()?.rootViewController = navController
        return navController
    }
    
    static private func getWindow() -> UIWindow? {
        var window: UIWindow? = nil
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            window = appDelegate.window
        }
        return window
    }
    
    static func getUserContacts() {
        let url = U_BASE2 + U_GET_CONTACTS + (DBManager.sharedInstance.currentUser[0].accNumber ?? "")
        SessionManager.shared.methodForApiCalling(url: url, method: .get, parameter: nil, objectClass: UsersData.self, requestCode: U_GET_CONTACTS, userToken: nil) { (response) in
            
            if (response.data?.count)! > 0 {
                //Save in DB
                DBManager.sharedInstance.addUserContacts(contacts: (response.data)!)
            }
            ActivityIndicator.hide()
        }
    }
    
    static func getTransactionHistory(transId: String) {
       // DispatchQueue.global(qos: .background).async {
            
            let url = U_BASE2 + U_GET_TRANSACTIONS + transId
            SessionManager.shared.methodForApiCalling(url: url, method: .get, parameter: nil, objectClass: TransactionData.self, requestCode: U_GET_TRANSACTIONS, userToken: nil) { (response) in
                print("response for transaction is:-",response)
                if (response.data?.count)! > 0 {
                    //Save in DB
                    DBManager.sharedInstance.addTransaction(trxList: response.data!, accNumber: nil)
                    NotificationCenter.default.post(name: Notification.Name("updateTransactionHistory"), object: nil)
                }
                
                if (response.data?.count)! == 11 {
                    self.getTransactionHistory(transId: response.data?.last?.trxid ?? "1.11.0")
                }
                ActivityIndicator.hide()
            }
            
      //  }
    }
    
    static func apiCallToGetSocial() {
        DispatchQueue.global(qos: .background).async {
            
            SessionManager.shared.methodForApiCalling(url: U_BASE+U_SOCIAL, method: .get, parameter: nil, objectClass: SocialsData.self, requestCode: U_SOCIAL, userToken: nil) { (response) in
                if let socials = response.socials, socials.count > 0 {
                    for social in socials {
                        if social.name == "Facebook" {
                            DBManager.sharedInstance.addSocial(name: social.name, imageName: "fb", url: social.link)
                            //                        community.append(Settings(icon: #imageLiteral(resourceName: "fb"), name: "Facebook", url: social.link))
                        } else if social.name == "Twitter" {
                            DBManager.sharedInstance.addSocial(name: social.name, imageName: "twitter", url: social.link)
                            //                        community.append(Settings(icon: #imageLiteral(resourceName: "twitter"), name: "Twitter", url: social.link))
                        } else if social.name == "Linkedin" {
                            DBManager.sharedInstance.addSocial(name: social.name, imageName: "linkedin-60", url: social.link)
                            //                        community.append(Settings(icon: #imageLiteral(resourceName: "linkedin-60.png"), name: "Linkedin", url: social.link))
                        }
                    }
                }
                ActivityIndicator.hide()
            }
        }
    }
}
