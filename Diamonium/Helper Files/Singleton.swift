//
//  Singleton.swift
//  Diamonium
//


import Foundation
import UIKit
import LocalAuthentication

class Singleton {
    
    static let shared = Singleton()
    
    private init() {
    }

//    var username: String {
//        guard let name = UserDefaults.standard.string(forKey: K_USERNAME) else {
//            return ""
//        }
//        return name.lowercased()
//    }
//    
//    var fullName: String {
//        guard let name = UserDefaults.standard.string(forKey: K_FULL_NAME) else {
//            return ""
//        }
//        return name
//    }
//    
//    var businessType: String {
//        guard let businessType = UserDefaults.standard.string(forKey: K_BUSINESS_TYPE) else {
//            return ""
//        }
//        return businessType
//    }
    
    var balance: String {
        guard let value = DBManager.sharedInstance.currentUser[0].currencyBalance else {
            return "0.0"
        }
        return value
    }
    
    var tokenBalance: String {
        guard let value = DBManager.sharedInstance.currentUser[0].bteBalance else {
            return "0.0"
        }
        return value
    }
    
    var userToken: String {
        return UserDefaults.standard.string(forKey: K_TOKEN) ?? ""
    }
    
    var firebaseToken: String {
        return UserDefaults.standard.string(forKey: K_FIREBASE_TOKEN) ?? ""
    }
    
    var selectedCurrency: Currency {
        let userDetail = DBManager.sharedInstance.currentUser[0]
        let currency: Currency = Currency(description: userDetail.currencyDescription, code: userDetail.currencyCode, symbol: userDetail.currencySymbol, isSelected: true)
        return currency
//        guard let decoded = UserDefaults.standard.value(forKey: K_CURRENCY_DETAIL) as? Data else {
//            return Currency(description: "AUD - Australian Dollar", code: "AUD", symbol: "$", isSelected: true)
//        }
//        let jsonDecoder = JSONDecoder()
//        return try! jsonDecoder.decode(Currency.self, from: decoded)
    }
    
//    func saveCurrency(detail: Currency) {
//        let jsonEncoder = JSONEncoder()
//        
//        if let encodedData = try? jsonEncoder.encode(detail) {
//            UserDefaults.standard.set(encodedData, forKey: K_CURRENCY_DETAIL)
//            UserDefaults.standard.synchronize()
//        }
//    }
    
    var autoLockTimer: TimeInterval? {
        set {
            UserDefaults.standard.set(newValue, forKey: K_TIMER)
        } get {
            return (UserDefaults.standard.value(forKey: K_TIMER) as? TimeInterval) ?? 0
        }
    }
    
    var enableAuthentication: Bool {
        set {
            UserDefaults.standard.set(newValue, forKey: K_AUTH)
        } get {
            return UserDefaults.standard.bool(forKey: K_AUTH)
        }
    }
    
    func cancelAuth() {
        
    }

    
    func authenticationWithTouchID(cancel: Bool, completionHandler: @escaping () -> Void) {
        let context = LAContext()
        context.localizedFallbackTitle = "Use Passcode"
        
        var authError: NSError?
        let reasonString = "To access the secure data"
        
        if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &authError) {
            context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reasonString) { (success, error) in
                if success {
                    completionHandler()
                    print("Authenticated successfully")
                } else {
                    guard let err = error else {
                        return
                    }
                    print(err.localizedDescription)
                    if !cancel {
                        DispatchQueue.main.async {
                            self.showAlert({
                                completionHandler()
                            })
                        }
                    }
                }
            }
        }else {
              if(passCodeScreen == "AmountScreen"){
                NotificationCenter.default.post(name: NSNotification.Name(N_PRESENT_PASSCODE), object: nil)
              }else {
                DispatchQueue.main.async {
                    Router.launchPasscodeVC()
                }
            }
        }
    }
    
    func showAlert(_ completionHandler: @escaping () -> Void) {
        var alertController = UIAlertController(title: "Please Authenticate", message: "Please unlock to continue.", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
            Singleton.shared.authenticationWithTouchID(cancel: false, completionHandler: {
                completionHandler()
            })
        }))
        UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
}
