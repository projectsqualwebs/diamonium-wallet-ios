//
//  DBManager.swift
//  Diamonium


import UIKit
import RealmSwift

class DBManager {
    
    static let sharedInstance = DBManager()
    
    private var database: Realm
    
    private init() {
        database = try! Realm()
        
    }
    
    var currentUser: Results<DBUsers> {
        return (database.objects(DBUsers.self).filter("token == '\(Singleton.shared.userToken)'"))
    }
    
    func getUserFromToken(_ token: String) -> DBUsers {
        return (database.objects(DBUsers.self).filter("token == '\(token)'"))[0]
    }
    
    func getUserFromUsername(_ accNumber: String?) -> DBUsers {
//        guard let accountNo = accNumber else {
//            return (database.objects(DBUsers.self).filter("accNumber == '\(accNumber!.characters.suffix(10))'"))[0]
//        }
        let user = (database.objects(DBUsers.self).filter("accNumber == '\(accNumber!)'"))
        if(user.count > 0){
         return user[0]
        }else {
         return DBUsers()
        }
    }
    
    func userAvailability(accNumber: String) -> Bool {
        let user = (database.objects(DBUsers.self).filter("accNumber == '\(accNumber)'"))
        if user.count > 0 {
            return true
        } else {
            return false
        }
    }
    
    func deleteTransactions() {
         try? database.write {
        let transaction = database.objects(DBTransaction.self).filter("username == '\(DBManager.sharedInstance.currentUser[0].accNumber!)'")
        database.delete(transaction)
        }
    }
    
    func deleteContact(name:String) {
        try? database.write {
            let deleteConatact = database.objects(DBContacts.self).filter("name == '\(name)'")
            database.delete(deleteConatact)
        }
    }
    
    func userContactAvailability(name: String) -> Bool {
        do{
            if(currentUser.count > 0){
                let user = currentUser[0].contacts.filter("name == '\(name)'")
                if user.count > 0 {
                    return true
                } else {
                    return false
                }
            }else {
                return false
            }
        }catch{
            print("error")
        }
    }
    
    //Users DB
    func getUsersFromDB() -> Results<DBUsers> {
        let results = database.objects(DBUsers.self)
        return results
    }
    
    func addUser(name: String?, email: String?, phone: String?, token: String?, key: String?, accType: String?, accNumber: String?, profileImage: String?, bteBalance: String?, currencyBalance: String?) {
        let user = DBUsers()
        user.name = name
        user.email = email
        user.phone = phone
        user.token = token
        user.key = key
        user.accType = accType
        user.accNumber = accNumber
        user.profileImage = profileImage
        user.bteBalance = bteBalance
        user.currencyBalance = currencyBalance

        try? database.write {
            database.add(user)
        }
    }
    
    func addUserContacts(contacts: [Users]) {
        
        for contact in contacts {
            let dbContact = DBContacts()
            dbContact.id = "\(contact.id!)"
            dbContact.name =  (contact.contact_name != nil) ? contact.contact_name:contact.name
            dbContact.contact_name = contact.name ?? ""
            dbContact.full_name = contact.full_name ?? ""
            dbContact.profileImage = contact.profile_image
            try? database.write {
                if(currentUser.count > 0){
                    currentUser[0].contacts.append(dbContact)
                }
            }
        }
    }
    
    func getContactsFromDB() -> List<DBContacts> {
        do{
            var results = List<DBContacts>()
            if(currentUser.count > 0){
                results =  currentUser[0].contacts
                return results
            }else {
                return results
            }
        }catch{
            print("error")
        }
    }
    
    func updateUserData(name: String?, email: String?, imagePath: String?, currency: Currency?, bteBalance: String?, currencyBalance: String?, accNumber: String? = nil) {
        
        let user = ((accNumber == nil) ? (DBManager.sharedInstance.currentUser[0]) : (self.getUserFromUsername(accNumber!)))
        
        if let username = name {
            try? database.write {
                user.name = username
                user.email = email
                user.profileImage = imagePath ?? ""
            }
        }
        
        if let bteBal = bteBalance {
            try? database.write {
                user.bteBalance = bteBal
                user.currencyBalance = currencyBalance
            }
        }
       
        if let currencyBal = currencyBalance {
            try? database.write {
                user.currencyBalance = currencyBal
            }
        }
        
        if let currencyDetail = currency {
            try? database.write {
                user.currencyDescription = currencyDetail.description
                user.currencyCode = currencyDetail.code
                user.currencySymbol = currencyDetail.symbol
            }
        }
    }
    
    //Transaction DB
    func getTransactionFromDB() -> Results<DBTransaction> {
        let results = database.objects(DBTransaction.self).filter("username == '\(DBManager.sharedInstance.currentUser[0].accNumber!)'")
        return results
    }
    
    func getLastTransactionId() -> String? {
        do {
            if(DBManager.sharedInstance.currentUser.count > 0){
                let results = database.objects(DBTransaction.self).filter("username == '\(DBManager.sharedInstance.currentUser[0].accNumber!)'").sorted(byKeyPath: "dateTime", ascending: false)
                if(results.count > 0){
                    return results[0].trxid
                }else {
                    return ""
                }
            }else {
                return ""
            }
        }catch{
        return ""
        print("error")
    }
}
    
    func addTransaction(trxList: [Transaction], accNumber: String?) {
        var trxCount: Int = 0
        if trxList.count == 11 {
            trxCount = 10
        } else {
            trxCount = trxList.count
        }
        for index in 0..<trxCount {
            if (trxList[index].type != 3) {
                let transaction = DBTransaction()
                transaction.name = trxList[index].name
                transaction.trxid = trxList[index].trxid
                transaction.amount = trxList[index].amount?.description
                transaction.type = trxList[index].type?.description
                transaction.profileImage = ""
                transaction.blockId = trxList[index].block_id?.description
                transaction.dateTime = trxList[index].datetime ?? 0
                transaction.username = accNumber ?? (currentUser.count > 0 ? currentUser[0].accNumber: "" )
                transaction.memoText =  trxList[index].memoText ?? ""
                transaction.authorizer = trxList[index].authorizer ?? ""
                transaction.toName = trxList[index].to_name ?? ""
                transaction.assetId = trxList[index].assetId ?? ""
                try? database.write {
                    database.add(transaction)
                }
            }
        }
    }
    
    //Delete DB
    func deleteAllFromDB() {
        try? database.write {
            database.deleteAll()
        }
    }
    
    //Social DB
    func getSocialFromDB() -> Results<DBSocial> {
        let results = database.objects(DBSocial.self)
        return results
    }
    
    func addSocial(name: String?, imageName: String?, url: String?) {
        let social = DBSocial()
        social.name = name
        social.imageName = imageName
        social.url = url
        
        try? database.write {
            database.add(social, update: .all)
        }
    }
    
    //News feeds DB
    func getClosedFeedsFromDB() -> Results<DBNewsFeed> {
        let results = database.objects(DBNewsFeed.self)
        return results
    }
    
    func addClosedFeeds(newsFeed: NewsFeeds?) {
        let feeds = DBNewsFeed()
        feeds.id = newsFeed?.id?.description
        feeds.title = newsFeed?.title
        feeds.timestamp = newsFeed?.created_timestamp?.description
        feeds.feedDescription = newsFeed?.description
        feeds.url = newsFeed?.url
        
        try? database.write {
            database.add(feeds)
        }
    }
}
