//
//  Objects.swift
//  Diamonium

import Foundation
import RealmSwift

class DBUsers: Object {
    
    @objc dynamic var name: String?
    @objc dynamic var email: String?
    @objc dynamic var phone: String?
    @objc dynamic var profileImage: String?
    @objc dynamic var accType: String?
    @objc dynamic var accNumber: String?
    @objc dynamic var token: String?
    @objc dynamic var key: String?
    @objc dynamic var bteBalance: String?
    @objc dynamic var currencyBalance: String?
    @objc dynamic var currencyDescription: String = "AUD - Australian Dollar"
    @objc dynamic var currencyCode: String = "AUD"
    @objc dynamic var currencySymbol: String = "$"
    var contacts = List<DBContacts>()
}

class DBContacts: Object {
    @objc dynamic var id: String?
    @objc dynamic var name: String?
    @objc dynamic var full_name: String?
    @objc dynamic var contact_name: String?
    @objc dynamic var profileImage: String?
}

class DBNewsFeed: Object {
    
    @objc dynamic var id: String?
    @objc dynamic var title: String?
    @objc dynamic var feedDescription: String?
    @objc dynamic var url: String?
    @objc dynamic var timestamp: String?
}

class DBTransaction: Object {
    
    @objc dynamic var username: String?
    @objc dynamic var trxid: String?
    @objc dynamic var name: String?
    @objc dynamic var profileImage: String?
    @objc dynamic var amount: String?
    @objc dynamic var type: String?
    @objc dynamic var blockId: String?
    @objc dynamic var memoText: String?
    @objc dynamic var authorizer: String?
    @objc dynamic var toName: String?
    @objc dynamic var dateTime = 0
    @objc dynamic var assetId: String?
}

class DBSocial: Object {
    
    @objc dynamic var name: String?
    @objc dynamic var imageName: String?
    @objc dynamic var url: String?
    
    override static func primaryKey() -> String? {
        return "name"
    }
}
