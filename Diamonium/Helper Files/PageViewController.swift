//
//  PageViewController.swift
//  Chalein
//
//  Created by User on 18/05/17.
//

import UIKit

protocol ControllerIndexDelegate {
    func getControllerIndex(index: Int)
}

class PageViewController: UIPageViewController
{
    static var dataSource1: UIPageViewControllerDataSource?
    static var index_delegate: ControllerIndexDelegate? = nil
//    static var controller: String = S_MY_BOOK
    var firstControllerPart: [UIViewController] = []
    var orderedViewControllers: [UIViewController] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SinglePageViewController.controller = K_HOME_SLIDER
        
//        if let myView = view?.subviews.last as? UIScrollView {
//            myView.canCancelContentTouches = false
//        }
        
        dataSource = self
        handleView()
        setControllers()
        PageViewController.dataSource1 = self
    }
    
    func setControllers() {
        if let lastViewController = orderedViewControllers.last {
            setViewControllers([lastViewController], direction: .forward, animated: true, completion: nil)
        }
        setViewControllers([orderedViewControllers[2]], direction: .forward, animated: true, completion: nil)
        setViewControllers([orderedViewControllers[1]], direction: .forward, animated: true, completion: nil)
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewDidAppear(_ animated: Bool) {
        PageViewController.dataSource1 = self
    }
    
    func handleView() {
        orderedViewControllers = [self.newColoredViewController(controller: "HomeViewController"),
                                  self.newColoredViewController(controller: "ReceiveViewController"),
                                  self.newColoredViewController(controller: "SendViewController"),
                                  self.newColoredViewController(controller: "SupportViewController")] 
    }
    
    func setControllerFirst() {
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .reverse,
                               animated: false,
                               completion: nil)
        }
    }
    
    func setControllerSecond() {
        setViewControllers([orderedViewControllers[1]], direction: .forward, animated: false, completion: nil)
    }
    
    func setControllerThird() {
        setViewControllers([orderedViewControllers[2]], direction: .forward, animated: false, completion: nil)
    }
    
    func setControllerLast() {
        if let lastViewController = orderedViewControllers.last {
            setViewControllers([lastViewController],
                               direction: .forward,
                               animated: false,
                               completion: nil)
        }
    }
    
    func currentControllerIndex(VC: UIViewController) {
        if let viewControllerIndex = orderedViewControllers.index(of: VC) {
            PageViewController.index_delegate?.getControllerIndex(index: viewControllerIndex)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     return [self.newColoredViewController(controller: "FindRideViewController"),
     self.newColoredViewController(controller: "OfferRideViewController")]
     // Pass the selected object to the new view controller.
     
     }
     */
//    private(set) lazy
    private func newColoredViewController(controller: String)->UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: controller)
    }
}

extension PageViewController: UIPageViewControllerDataSource
{
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        currentControllerIndex(VC: viewController)
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        let previousIndex = viewControllerIndex - 1
        guard previousIndex >= 0 else {
            return nil
        }
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        currentControllerIndex(VC: viewController)
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        let nextIndex = viewControllerIndex + 1
        guard orderedViewControllers.count != nextIndex else {
            return nil
        }
        guard orderedViewControllers.count > nextIndex else {
            return nil
        }
        return orderedViewControllers[nextIndex]
    }
}
