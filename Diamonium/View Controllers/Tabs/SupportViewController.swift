//
//  SupportViewController.swift
//  Diamonium
//
//  Created by AM on 17/09/19.
//

import UIKit
import SkyFloatingLabelTextField
import Alamofire

class SupportViewController: UIViewController {
    
    //MARK: IBOutlets
    
    @IBOutlet weak var email: SkyFloatingLabelTextField!
    @IBOutlet weak var fullName: SkyFloatingLabelTextField!
    @IBOutlet weak var supportTitle: SkyFloatingLabelTextField!
    @IBOutlet weak var desc: UITextView!
    @IBOutlet weak var documentimage: UIImageView!
    @IBOutlet weak var documentButton: UIButton!
    
    var imageName: String = ""
    let picker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.documentButton.setImage(UIImage(named: "copy"), for: .normal)
        if  (DBManager.sharedInstance.currentUser.count > 0) {
            self.email.text = DBManager.sharedInstance.currentUser[0].email
            
                self.fullName.text = DBManager.sharedInstance.currentUser[0].name
        }
    }
    
    
    
    @IBAction func submitForm(_ sender: Any) {
        if(self.email.text!.isEmpty){
            self.showAlert(title: nil, message: "Email address required", action1Name: "Ok", action2Name: nil)
        }else if !(self.isValidEmail(emailStr: self.email.text!)){
            self.showAlert(title: nil, message: "Enter valid email address", action1Name: "Ok", action2Name: nil)
        }else  if(self.fullName.text!.isEmpty){
            self.showAlert(title: nil, message: "Enter your full name", action1Name: "Ok", action2Name: nil)
        }else  if(self.supportTitle.text!.isEmpty){
            self.showAlert(title: nil, message: "Enter title", action1Name: "Ok", action2Name: nil)
        }else  if(self.desc.text!.isEmpty){
            self.showAlert(title: nil, message: "Enter description", action1Name: "Ok", action2Name: nil)
        }else  if(self.desc.text!.count < 20){
            self.showAlert(title: nil, message: "Enter atleast 20 characters", action1Name: "Ok", action2Name: nil)
        }else {
            ActivityIndicator.show(view: self.view)
            let language = "en"
            let captcha = "valid"
            let departId = "1"
            let departindex = "0"
            let supportTitle = self.supportTitle.text
            let myImage = self.documentimage.image
            let descript = self.desc.text
            let mail = self.email.text
            let name = self.fullName.text!
            
            let headers: HTTPHeaders = [
                /* "Authorization": "your_access_token",  in case you need authorization header */
                "Content-type": "multipart/form-data"
            ]
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                if let image = myImage?.pngData() {
                    multipartFormData.append (image, withName: "file" , fileName: self.imageName, mimeType: "image/jpg")
                }
                multipartFormData.append(Data(supportTitle!.utf8), withName: "title")
                 multipartFormData.append(Data(descript!.utf8), withName: "content")
                 multipartFormData.append(Data(departindex.utf8), withName: "departmentIndex")
                 multipartFormData.append(Data(mail!.utf8), withName: "email")
                 multipartFormData.append(Data(name.utf8), withName: "name")
                 multipartFormData.append(Data(language.utf8), withName: "language")
                 multipartFormData.append(Data(captcha.utf8), withName: "captcha")
                    multipartFormData.append(Data(departId.utf8), withName: "departmentId")
            }, usingThreshold: UInt64.init(), to: U_SUPPORT_FORM, method: .post, headers:nil) { (encodingResult) in
                
                switch encodingResult {
                case .success(let response,_,_):
                    response.responseString(completionHandler: { (dataResponse) in
                        
                        ActivityIndicator.hide()
                        
                        let errorObject = SessionManager.shared.convertDataToObject(response: dataResponse.data, Response.self)
                        
                        if dataResponse.response?.statusCode == 200 {
                            self.supportTitle.text = ""
                            self.desc.text = ""
                            self.documentimage.image = nil
                             self.documentButton.setImage(UIImage(named: "copy"), for: .normal)
                            let object = SessionManager.shared.convertDataToObject(response: dataResponse.data, Response.self)
                              UIApplication.shared.keyWindow?.rootViewController?.showAlert(title: "Ticket has been created.", message: object?.message, action1Name: "Ok", action2Name: nil)
                        } else {
                            
                            UIApplication.shared.keyWindow?.rootViewController?.showAlert(title: "Error", message: errorObject?.message, action1Name: "Ok", action2Name: nil)
                        }
                    })
                    break
                case .failure(let error):
                    //Showing error message on alert
                    UIApplication.shared.keyWindow?.rootViewController?.showAlert(title: "Error", message: error.localizedDescription, action1Name: "Ok", action2Name: nil)
                    break
                }
            }
            
            
            
            
            
            
//
//            SessionManager.shared.methodForApiCalling(url: U_SUPPORT_FORM, method: .post, parameter: param, objectClass: Response.self, requestCode: U_SUPPORT_FORM, userToken: nil) { (response) in
//

            
//            }
        }
    }
    
    
    @IBAction func selectFile(_ sender: UIButton) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
               alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                   self.openCamera()
               }))
               
               alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                   self.openGallary()
               }))
               
               alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
               if let popoverController = alert.popoverPresentationController {
                   popoverController.sourceView = sender as! UIView
                   popoverController.sourceRect = (sender as AnyObject).bounds
               }
               self.present(alert, animated: true, completion: nil)
    }

}

extension SupportViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextViewDelegate {
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        picker.allowsEditing = true
        self.present(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
        
        if let selectedImageName = ((info[UIImagePickerController.InfoKey.referenceURL] as? NSURL)?.lastPathComponent) {
            self.imageName = selectedImageName
        } else {
            self.imageName = "image.jpg"
        }
        self.documentimage.image = selectedImage
        self.documentButton.setImage(nil, for: .normal)
        picker.dismiss(animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
