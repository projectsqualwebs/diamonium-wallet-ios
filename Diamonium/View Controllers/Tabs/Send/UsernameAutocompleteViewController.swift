//
//  UsernameAutocompleteViewController.swift
//  Diamonium
//
//

import UIKit

class UsernameAutocompleteViewController: UIViewController {
    
    //MARK: IBOutlet
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableViewUsernames: UITableView!
    @IBOutlet weak var topBarHeightConstraint: NSLayoutConstraint!
    
    
    
    var usernameList: [Users]?
    var usernameDelegate: UsernameDelegate? = nil
    var contact: Bool = false
    var countryCode:String?
    
    var navBarHeight: CGFloat {
        get {
            let barHeight = self.navigationController?.navigationBar.frame.height ?? 44
            let statusBarHeight = UIApplication.shared.statusBarFrame.height
            return barHeight+statusBarHeight
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableViewUsernames.tableFooterView = UIView()
        topBarHeightConstraint.constant = navBarHeight
        self.searchBar.keyboardType = .phonePad
        self.searchBar.placeholder = "Enter Mobile Number"
        searchBar.becomeFirstResponder()
    }
    
    func apiCallToGetUsernames(text: String) {
        SessionManager.shared.methodForApiCalling(url: U_BASE2+U_GET_USERNAMES, method: .post, parameter: [K_KEYWORDS: text], objectClass: UsersData.self, requestCode: U_GET_USERNAMES, userToken: nil) { (response) in
           // ActivityIndicator.show(view: self.view)
            if (response.data?.count)! > 0 {
                self.usernameList = [Users]()
                self.usernameList = response.data
                self.tableViewUsernames.isHidden = false
                self.tableViewUsernames.reloadData()
            } else {
                self.tableViewUsernames.isHidden = true
            }
            ActivityIndicator.hide()
        }
        
    }

    //MARK: Action
    @IBAction func cancel(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}

extension UsernameAutocompleteViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count > 1 {
            let myText =  self.countryCode! + searchText
            var textFieldInsideSearchBar = self.searchBar.value(forKey: "searchField") as? UITextField
            textFieldInsideSearchBar?.textColor = .white
            
            apiCallToGetUsernames(text: myText)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if !((searchBar.text?.isEmpty)!) {
            let myText =  self.countryCode! +  searchBar.text!
            apiCallToGetUsernames(text:myText )
        }
        searchBar.resignFirstResponder()
    }
}
extension UsernameAutocompleteViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usernameList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "UsernameTableViewCell", for: indexPath) as! UsernameTableViewCell
        let user = usernameList?[indexPath.row] as? Users
//            if (DBManager.sharedInstance.currentUser[0].name == user?.name){
//                break
//            }
        cell.labelUsername.text = user?.name
        if let profile = user?.profile_image, profile != "" {
            cell.userProfile.sd_setImage(with: URL(string: (user?.profile_image)!)!, placeholderImage: #imageLiteral(resourceName: "defaultProfile.png"))
        } else {
            cell.userProfile.image = #imageLiteral(resourceName: "defaultProfile.png")
        }
        
        return cell
    }
}
extension UsernameAutocompleteViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let user = usernameList?[indexPath.row] as? Users else { return }
        
        if (user.name == DBManager.sharedInstance.currentUser[0].accNumber) {
            self.showAlert(title: nil, message: "You can't add own account in contact", action1Name: "Ok", action2Name: nil)
        } else if contact && (DBManager.sharedInstance.userContactAvailability(name: user.name!)) {
            self.showAlert(title: "Error", message: "Already added in your contacts.", action1Name: "Ok", action2Name: nil)
        } else {
            DispatchQueue.main.async {
                self.dismiss(animated: true) {
                    self.usernameDelegate?.getUsernameFromList(selectedUser: user, isContact: self.contact)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
