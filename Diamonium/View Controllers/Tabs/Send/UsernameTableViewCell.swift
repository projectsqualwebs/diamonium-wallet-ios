//
//  UsernameTableViewCell.swift
//  Diamonium
//
//

import UIKit

class UsernameTableViewCell: UITableViewCell {

    //MARK: IBOutlet
    @IBOutlet weak var userProfile: UIImageView!
    @IBOutlet weak var labelUsername: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
