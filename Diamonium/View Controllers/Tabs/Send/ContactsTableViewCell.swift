//
//  ContactsTableViewCell.swift
//  Diamonium
//
//

import UIKit

class ContactsTableViewCell: UITableViewCell {

    //MARK: IBOutlet
    @IBOutlet weak var profileImage: ImageView!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var name: UILabel!
    
    
    var deleteAction: (() -> Void)? = nil
    var sendAction: (() -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func sendMoneyAction(_ sender: UIButton) {
        if let tickAction = sendAction {
            tickAction()
        }
    }
    @IBAction func deleteContactAction(_ sender: UIButton) {
        if let tickAction = deleteAction {
            tickAction()
        }
    }

}
