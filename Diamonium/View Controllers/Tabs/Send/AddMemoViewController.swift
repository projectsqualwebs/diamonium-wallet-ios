//
//  AddMemoViewController.swift
//  Alamofire
//
//  Created by AM on 10/05/19.
//

import UIKit

class AddMemoViewController: UIViewController {
    
    @IBOutlet weak var textFieldForMemo: UITextField!
    
    var text: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textFieldForMemo.text = text ?? ""
    }
    
    @IBAction func okButtonAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("MemoText"), object:["message": self.textFieldForMemo.text])
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
