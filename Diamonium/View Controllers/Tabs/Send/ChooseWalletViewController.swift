//
//  ChooseWalletViewController.swift
//  Diamonium
//
//

import UIKit
import RealmSwift

class ChooseWalletViewController: UIViewController {
    //MARK: IBOUtlets
    @IBOutlet weak var tsbleView: UITableView!
    
    
    var transferTo = String()
    var beValue: String?
    var localValue: String?
    var accountList: Results<DBUsers>!
    var isTransferBetween = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tsbleView.tableFooterView = UIView()
        
        if (transferTo.isEmpty) && transferTo != "" {
            accountList = DBManager.sharedInstance.getUsersFromDB().filter("token != '\(Singleton.shared.userToken)'")
        } else {
            accountList = DBManager.sharedInstance.getUsersFromDB().filter("accNumber != '\(transferTo)'")
        }
    }
    
    //MARK: Action
    @IBAction func back(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
extension ChooseWalletViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(accountList != nil){
            return accountList.count
        }else {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WalletTableViewCell", for: indexPath) as! WalletTableViewCell
        
        let user = accountList[indexPath.row]
        cell.labelUsername.text = user.accNumber
        cell.labelAccType.text = user.bteBalance! + " DIA"
        if let image = user.profileImage, image != "" {
            cell.profileImage.sd_setImage(with: URL(string: user.profileImage!)!, placeholderImage: #imageLiteral(resourceName: "defaultProfile.png"))
        }
        return cell
    }
}
extension ChooseWalletViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let user = accountList[indexPath.row]
        
        let amountController = storyboard?.instantiateViewController(withIdentifier: "AmountViewController") as! AmountViewController
         if(isTransferBetween){
            amountController.user = DBManager.sharedInstance.currentUser[0]
            amountController.transferTo = user.accNumber
         }else if (transferTo.isEmpty){
            amountController.user = DBManager.sharedInstance.currentUser[0]
            amountController.transferTo = user.accNumber
         }else {
            amountController.user = user
            amountController.transferTo = transferTo
        }
        amountController.beValue = beValue
        amountController.localValue = localValue
        navigationController?.pushViewController(amountController, animated: true)
    }
}
