//
//  SendViewController.swift
//  Diamonium
//
//

import UIKit
import SkyFloatingLabelTextField
import RealmSwift
import FlagPhoneNumber


class SendViewController: UIViewController {
    
    //MARK: IBOutlet
    @IBOutlet weak var tableViewContacts: UITableView!
    @IBOutlet weak var labelNoContacts: UILabel!
    @IBOutlet weak var viewForDeletingContact: UIView!
    @IBOutlet weak var labelForDeletedUser: UILabel!
    @IBOutlet weak var labelForMobilePrefix: UILabel!
    @IBOutlet weak var textFieldSentTo: FPNTextField!
    
    var contactList = [Users]()
    var asset: String = "DIA"
    var availableBalance: Double?
    var deletingindex: Int?
    var countryCode: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textFieldSentTo.attributedPlaceholder = NSAttributedString(string:"Username", attributes:[NSAttributedString.Key.foregroundColor: UIColor.white])
        
        NotificationCenter.default.addObserver(self, selector: #selector(getUsernameByScan(_:)), name: NSNotification.Name("ScanQRCode"), object: nil)
        self.viewForDeletingContact.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(refreshFields), name: NSNotification.Name("refreshFields"), object: nil)
        labelForMobilePrefix.text = Locale.current.currencyCode
        textFieldSentTo.delegate = self
        labelForMobilePrefix.text = Locale.current.currencyCode
        countryCode = textFieldSentTo.selectedCountry?.phoneCode.replacingOccurrences(of: "+", with: "")
        textFieldSentTo.textColor = .white
        textFieldSentTo.enableMode = .enabled
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getContacts()
    }
    
    func getContacts() {
        contactList = [Users]()
        let contacts = DBManager.sharedInstance.currentUser[0].contacts
        
        for contact in contacts {
            contactList.append(Users(id:Int(contact.id!), name: contact.name, image: contact.profileImage,full_name: contact.full_name))
        }
        manageContactList()
    }
    
    func manageContactList() {
        if contactList.count > 0 {
            tableViewContacts.isHidden = false
            labelNoContacts.isHidden = true
            tableViewContacts.reloadData()
        } else {
            labelNoContacts.isHidden = false
            tableViewContacts.isHidden = true
        }
    }
    
    @objc func getUsernameByScan(_ notification: NSNotification) {
           NotificationCenter.default.removeObserver(self, name: NSNotification.Name("ScanQRCode"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getUsernameByScan(_:)), name: NSNotification.Name("ScanQRCode"), object: nil)
        if let object = notification.object as? String {
            let nameAmount = object.components(separatedBy: "+")
            DispatchQueue.main.async {
                let chooseWalletContoller = self.storyboard?.instantiateViewController(withIdentifier: "ChooseWalletViewController") as! ChooseWalletViewController
                if(nameAmount.count > 1){
                    chooseWalletContoller.transferTo = nameAmount[0]
                    chooseWalletContoller.beValue = nameAmount[1]
                    chooseWalletContoller.localValue = nameAmount[2]
                    self.navigationController?.pushViewController(chooseWalletContoller, animated: true)
                }else {
                    self.showAlert(title: nil, message: "Invalid QR Code", action1Name: "Ok", action2Name: nil)
                }
            }
        }
          
    }
    
    @objc func refreshFields() {
        textFieldSentTo.text = ""
    }
    
    //    func attributedString(_ value: String?, ext: String, error: Bool) -> NSMutableAttributedString {
    //        ext.lowercased()
    //        var attrValue: [NSAttributedString.Key : Any]?
    //        var completeString:String?
    //        if error {
    //            attrValue = [NSAttributedString.Key.foregroundColor: UIColor.red]
    ////            "Warning! only %@ %@ is available"
    //            completeString  = String(format: "Insufficient Amount", amount, ext)
    //            return NSMutableAttributedString(string: completeString!, attributes: attrValue)
    //        } else {
    //            attrValue = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
    //            var newSTring = NSMutableAttributedString(string: value!, attributes: attrValue)
    //            newSTring.append(NSAttributedString(string: " \(ext.uppercased()) available"))
    //            return newSTring
    //        }
    //    }
    
    func showAssetAvailability(assetEnable: CustomButton, assetDisable: CustomButton) {
        assetEnable.borderColor = primaryColor
        assetEnable.setTitleColor(primaryColor, for: .normal)
        assetDisable.borderColor = .black
        assetDisable.setTitleColor(.black, for: .normal)
        assetEnable.borderWidth = 2.0
        assetDisable.borderWidth = 1.0
    }
    
    func apiCallToAddContacts(selectedUser: Users) {
        ActivityIndicator.show(view: self.view)
        let param = [K_USERNAME: DBManager.sharedInstance.currentUser[0].accNumber!,
                     K_CONTACT_NAME: selectedUser.name!]
        SessionManager.shared.methodForApiCalling(url: U_BASE2+U_ADD_CONTACT, method: .post, parameter: param, objectClass: Response.self, requestCode: U_ADD_CONTACT, userToken: nil) { (response) in
            
            self.contactList.append(Users(id:selectedUser.id,name: selectedUser.name,image: selectedUser.profile_image,full_name: selectedUser.full_name))
            self.manageContactList()
            DBManager.sharedInstance.addUserContacts(contacts: [selectedUser])
            ActivityIndicator.hide()
        }
    }
    
    //MARK: Action
    @IBAction func getUsernameList(_ sender: UIButton) {
        let VC = storyboard?.instantiateViewController(withIdentifier: "UsernameAutocompleteViewController") as! UsernameAutocompleteViewController
        VC.countryCode = ((self.labelForMobilePrefix.text)?.lowercased())! + "-" + (self.countryCode ?? "")
        VC.usernameDelegate = self
        VC.contact = false
        present(VC, animated: true, completion: nil)
    }
    
    @IBAction func addContact(_ sender: UIButton) {
        let VC = storyboard?.instantiateViewController(withIdentifier: "UsernameAutocompleteViewController") as! UsernameAutocompleteViewController
        VC.usernameDelegate = self
        VC.countryCode = ((self.labelForMobilePrefix.text)?.lowercased())! + "-" + (self.countryCode ?? "")
        VC.contact = true
        present(VC, animated: true, completion: nil)
    }
    
    @IBAction func pasteClipboard(_ sender: UIButton) {
        if let pasteStr = UIPasteboard.general.string {
            textFieldSentTo.text = pasteStr
            
            SessionManager.shared.createWallet = false
            apiCallToValidateUser(pasteStr) { (validate) in
                if validate {
                    let chooseWalletContoller = self.storyboard?.instantiateViewController(withIdentifier: "ChooseWalletViewController") as! ChooseWalletViewController
                    chooseWalletContoller.transferTo = pasteStr
                    self.navigationController?.pushViewController(chooseWalletContoller, animated: true)
                }
            }
        }
    }
    
    @IBAction func chooseWallet(_ sender: UIButton) {
        let chooseWalletContoller = self.storyboard?.instantiateViewController(withIdentifier: "ChooseWalletViewController") as! ChooseWalletViewController
        chooseWalletContoller.transferTo = DBManager.sharedInstance.currentUser[0].accNumber ?? ""
        chooseWalletContoller.isTransferBetween = true
        navigationController?.pushViewController(chooseWalletContoller, animated: true)
    }
    
    @IBAction func scanQrCode(_ sender: UIButton) {
        let scanContoller = self.storyboard?.instantiateViewController(withIdentifier: "ScanViewController") as! ScanViewController
        self.present(scanContoller, animated: true, completion: nil)
    }
    
    @IBAction func okButtonAction(_ sender: Any) {
        self.deleteContact(id:"\(self.contactList[self.deletingindex!].contact_name!)")
        self.viewForDeletingContact.isHidden = true
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.viewForDeletingContact.isHidden = true
    }
    
    @IBAction func hideDeleteViewAction(_ sender: Any) {
        self.viewForDeletingContact.isHidden = true
    }
}
extension SendViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactsTableViewCell", for: indexPath) as! ContactsTableViewCell
        let contact = contactList[indexPath.row]
        
        cell.labelUsername.text = contact.full_name
        cell.name.text = contact.name
        if let image = contact.profile_image, image != "" {
            cell.profileImage.sd_setImage(with: URL(string: image)!, placeholderImage: #imageLiteral(resourceName: "defaultProfile.png"))
        } else {
            cell.profileImage.image = #imageLiteral(resourceName: "defaultProfile.png")
        }
        cell.deleteAction = {
            self.labelForDeletedUser.text = "Are you sure to remove \(contact.contact_name!) from contacts."
            self.viewForDeletingContact.isHidden = false
            self.deletingindex = indexPath.row
            
        }
        
        cell.sendAction = {
            let chooseWalletContoller = self.storyboard?.instantiateViewController(withIdentifier: "ChooseWalletViewController") as! ChooseWalletViewController
            chooseWalletContoller.transferTo = self.contactList[indexPath.row].name ?? ""
            self.navigationController?.pushViewController(chooseWalletContoller, animated: true)
        }
        
        return cell
    }
    
    func  deleteContact(id:String) {
        ActivityIndicator.show(view: self.view)
        DBManager.sharedInstance.deleteContact(name: id)
        getContacts()
        SessionManager.shared.methodForApiCalling(url: U_BASE2+U_DELETE_CONTACT + id, method: .get, parameter: nil, objectClass: UsersData.self, requestCode: U_DELETE_CONTACT, userToken: nil) { (response) in
            ActivityIndicator.hide()
        }
    }
}
extension SendViewController: UITableViewDelegate {
    //    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //
    //    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
extension SendViewController: UsernameDelegate {
    func getUsernameFromList(selectedUser: Users, isContact: Bool) {
        if isContact {
            DispatchQueue.global(qos: .background).sync {
                self.apiCallToAddContacts(selectedUser: selectedUser)
            }
        } else {
            textFieldSentTo.text = selectedUser.name
            let chooseWalletContoller = self.storyboard?.instantiateViewController(withIdentifier: "ChooseWalletViewController") as! ChooseWalletViewController
            chooseWalletContoller.transferTo = selectedUser.name ?? ""
            navigationController?.pushViewController(chooseWalletContoller, animated: true)
        }
    }
}

extension SendViewController: FPNTextFieldDelegate {
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        return
    }
    
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        countryCode = dialCode.replacingOccurrences(of: "+", with: "")
        let currency = (Locale.currency[code])!
        if let currencyCode = (currency as? (String, String))?.0 {
            labelForMobilePrefix.text = currencyCode
        }
        
    }
}
