//
//  AmountViewController.swift
//  Diamonium
//
//

import UIKit
import LocalAuthentication



class AmountViewController: UIViewController {

    //MARK: IBOutlet
    @IBOutlet weak var labelBalance1: UILabel!
    @IBOutlet weak var labelBalanceTypeFrom: UILabel!
    @IBOutlet weak var labelBalance2: UILabel!
    @IBOutlet weak var labelBalanceTypeTo: UILabel!
    @IBOutlet weak var labelBalanceNow: UILabel!
    @IBOutlet weak var labelBalAfter: UILabel!
    
    @IBOutlet weak var buttonForCancel: UIButton!
    @IBOutlet weak var transferMainPopUp: UIView!
    
    @IBOutlet weak var finalPreviewPopUp: View!
    @IBOutlet weak var labelFrom: UILabel!
    @IBOutlet weak var labelTo: UILabel!
    @IBOutlet weak var labelAmount: UILabel!
    @IBOutlet weak var memoLabel: UIButton!
    
    
    @IBOutlet weak var transferConfirmPopUp: View!
    @IBOutlet weak var labeConfirmFrom: UILabel!
    @IBOutlet weak var labelConfirmTo: UILabel!
    @IBOutlet weak var labelConfirmAmount: UILabel!
    @IBOutlet weak var labelBlockNumber: UILabel!
    
    var user: DBUsers?
    var transferTo: String?
    var beValue: String?
    var localValue: String?
    var showBTE: Bool = false
    var amount: Double = 0
    var userToken: String?
    var memoText: String?
    var isDecimal: Bool? = false
    var countDecimal = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        transferMainPopUp.alpha = 0
        userToken = user?.token
        
        if beValue != nil {
            amount = Double(beValue ?? "0")!
            labelBalance1.text = localValue
            labelBalance2.text = beValue
            labelBalanceTypeFrom.text = user?.currencyCode
            
            let balAfter = String(format: "%.2f", ((Double(user!.bteBalance!)!) - (K_TRANSFER_FEES + (Double(beValue!)!))))
            
            labelBalanceNow.text = "BALANCE NOW: " + (user?.bteBalance)! + " DIA"
            labelBalAfter.text = "BALANCE AFTER: " + balAfter + " DIA"
        } else {
            handleBalance(switchCurrency: false)
        }
    }
    
    func handleBalance(switchCurrency: Bool) {
        labelBalanceNow.text = "BALANCE NOW: " + (user?.bteBalance)! + " DIA"
        labelBalAfter.text = "BALANCE AFTER: " + (user?.bteBalance)! + " DIA"

        labelBalance1.text = "0"
        if showBTE {
            labelBalanceTypeFrom.text = "DIA"
            labelBalance2.text = "0"
            labelBalanceTypeTo.text = user?.currencyCode
        } else {
            labelBalanceTypeFrom.text = user?.currencyCode
            labelBalance2.text = "0"
            labelBalanceTypeTo.text = "DIA"
        }
    }
    
    func showPopUp(view: UIView, fundTransfer: FundTransfer?) {
        
        if view == finalPreviewPopUp {
            buttonForCancel.isHidden = false
            finalPreviewPopUp.isHidden = false
            transferConfirmPopUp.isHidden = true
            labelFrom.text = user?.accNumber
            labelTo.text = transferTo
            labelAmount.text = amount.description + " DIA"
        } else {
            buttonForCancel.isHidden = true
            finalPreviewPopUp.isHidden = true
            transferConfirmPopUp.isHidden = false
            labeConfirmFrom.text = fundTransfer?.name
            labelConfirmTo.text = fundTransfer?.to_name
            labelConfirmAmount.text = "\(fundTransfer!.amount!) DIA"
        }
        
        transferMainPopUp.alpha = 0.3
        view.alpha = 0.5
        view.transform = CGAffineTransform(scaleX: 0.4, y: 0.4)
        
        UIView.animate(withDuration: 0.5) {
            self.transferMainPopUp.alpha = 1
            view.alpha = 1
            view.transform = CGAffineTransform(scaleX: 1, y: 1)
        }
    }
    
    @IBAction func cancelFinalPreviewAction(_ sender: Any) {
      self.transferMainPopUp.alpha = 0
      finalPreviewPopUp.isHidden = true
      transferConfirmPopUp.isHidden = true
    }
    
    
    @IBAction func addMemoAction(_ sender: Any) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.addMemotText(notification:)), name: Notification.Name("MemoText"), object: nil)
        let myVC =
            self.storyboard?.instantiateViewController(withIdentifier: "AddMemoViewController") as! AddMemoViewController
        myVC.text = self.memoText ?? ""
        self.present(myVC, animated: true, completion: nil)
    }
    
    @objc func addMemotText(notification: Notification) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("MemoText"), object: nil)
        let dict = notification.object as! NSDictionary
        if( dict["message"] as! String != ""){
          self.memoLabel.setTitle("Edit Memo", for: .normal)
        }
        self.memoText = dict["message"] as! String
    }
    
    
    func hidePopUp(view: UIView, popBack: Bool) {
        UIView.animate(withDuration: 0.5, animations: {
            self.transferMainPopUp.alpha = 0
            view.alpha = 0
            view.transform = CGAffineTransform(scaleX: 0.4, y: 0.4)
        }) { (_) in
            if popBack {
                NotificationCenter.default.post(name: NSNotification.Name("refreshFields"), object: nil)
                self.popToTabController()
            }
        }
    }
    
    func addTransactionToList(fund:FundTransfer) {
        let timeInterval: Int = Int(Date().timeIntervalSince1970)
        DBManager.sharedInstance.addTransaction(trxList: [Transaction(id: fund.trxid, name: fund.to_name, dateTime: timeInterval, amount: "\(self.amount)", type: K_SENT, blockId:fund.block_id!,memoText:self.memoText,to_name: fund.to_name,authorizer: fund.authorizer, assetId: fund.assetId ?? "")], accNumber: fund.name)
    }
    
    func apiCallToTransfer() {
        let param = [K_ASSET: "dia", K_AMOUNT: amount.description, K_TO: transferTo!, K_MEMO: ""]
        SessionManager.shared.methodForApiCalling(url: U_BASE2+U_TRANSFER, method: .post, parameter: param, objectClass: FundTransfer.self, requestCode: U_TRANSFER, userToken: self.userToken) { (fundTransfer) in
            self.labelBlockNumber.text = "\(fundTransfer.block_id!)"
            self.addTransactionToList(fund: fundTransfer)
            self.memoLabel.setTitle("Add Memo", for: .normal)
            SessionManager.shared.apiCallToGetBalance(completionHandler: { (balance, tokenBalance) in
                DispatchQueue.main.async {
                    DBManager.sharedInstance.updateUserData(name: nil, email: nil, imagePath: nil, currency: nil, bteBalance: tokenBalance, currencyBalance: balance)
                    self.showPopUp(view: self.transferConfirmPopUp, fundTransfer: fundTransfer)
                   NotificationCenter.default.post(name: NSNotification.Name("updateBalance"), object: self.userToken)
                }
            })
        }
    }
    
    func apiCallToConvertCurrency() {
        let param = [K_AMOUNT: Double((self.labelBalance1.text)!),
                     K_CURRENCY_FROM: self.labelBalanceTypeFrom.text!,
                     K_CURRENCY_TO: self.labelBalanceTypeTo.text!] as [String : Any]
        SessionManager.shared.methodForApiCalling(url: U_BASE2+U_CURRENCY_CONVERT, method: .post, parameter: param, objectClass: CurrencyBalances.self, requestCode: U_CURRENCY_CONVERT, userToken: nil) { (balance) in
            let bteBalance = Double((self.user?.bteBalance)!)! - (balance.be_value ?? 0) - K_TRANSFER_FEES
            let currencyBalance = String(format: "%.2f", bteBalance)
            self.amount = Double(String(format: "%.2f", (balance.be_value) ?? 0))!
            self.labelBalAfter.text = "BALANCE AFTER: " + currencyBalance + " DIA"
            
            if !(self.showBTE) {
                if (self.labelBalance1.text == "0"){
                    self.labelBalance2.text = "0"
                }else{
                  self.labelBalance2.text = String(format: "%.2f", (balance.be_value) ?? 0)
                }
            } else {
                if (self.labelBalance1.text == "0"){
                    self.labelBalance2.text = "0"
                }else{
                    self.labelBalance2.text = String(format: "%.2f", (balance.local_value) ?? 0)
                }
            }
            ActivityIndicator.hide()
        }
        
    }
    
    //MARK: Action
    @IBAction func customKeyboard(_ sender: UIButton) {
        let value = labelBalance1.text!+sender.tag.description
        if let myVal = value.double {
            if sender.tag == 11 {
                if (labelBalance1.text?.count)! == 1 {
                    labelBalance1.text = "0"
                    self.isDecimal = false
                    self.countDecimal = 0
                }else {
                    labelBalance1.text = labelBalance1.text?.dropLast().description
                    if(self.isDecimal!){
                        countDecimal = countDecimal - 1
                    }
                }
                self.apiCallToConvertCurrency()
            } else if sender.tag == 10 {
                labelBalance1.text! += "."
                self.isDecimal = true
                countDecimal = 0
            } else if ((Double(value)! > (Double(user!.bteBalance!)! - K_TRANSFER_FEES)) && showBTE) || ((Double(value)! > Double(user!.currencyBalance!)!) && !showBTE) {
                showAlert(title: "Error", message: "Entered value should not be greater than available balance", action1Name: "Ok", action2Name: nil)
                return
            } else {
                if labelBalance1.text == "0" || labelBalance1.text == "" {
                    labelBalance1.text = sender.tag.description
                } else {
                    if(self.isDecimal!){
                        if (countDecimal <= 1){
                            labelBalance1.text! += sender.tag.description
                            countDecimal = countDecimal + 1
                        }else {
                            labelBalance1.text! += ""
                        }
                    }else {
                        labelBalance1.text! += sender.tag.description
                        self.isDecimal = false
                        countDecimal = 0
                    }
                }
                self.apiCallToConvertCurrency()
            }
        }else {
            labelBalance1.text! = "0"
        }
    }
    
    @IBAction func pasteAmount(_ sender: UIButton) {
        if let value = UIPasteboard.general.string, let validNumber = Double(value) {
            if ((validNumber > Double(user!.bteBalance!)!) && showBTE) || ((validNumber > Double(user!.currencyBalance!)!) && !showBTE) {
                showAlert(title: "Error", message: "Entered value should not be greater than available balance", action1Name: "Ok", action2Name: nil)
            } else {
                self.labelBalance1.text = validNumber.description
                self.apiCallToConvertCurrency()
            }
        } else {
            self.showAlert(title: "Error", message: "Please paste valid amount", action1Name: "Ok", action2Name: nil)
        }
    }
    
    @IBAction func switchCurrency(_ sender: Any) {
        showBTE = !showBTE
        handleBalance(switchCurrency: true)
    }
    
    @IBAction func transferAllBalance(_ sender: Any) {
        let bal = Double((user?.bteBalance) ?? "0")! - 20.0
        amount = Double(String(format: "%.2f", bal))!
        labelBalance1.text = String(format: "%.2f", bal)
        labelBalanceTypeFrom.text = "DIA"
        labelBalance2.text = user?.currencyBalance
        labelBalanceTypeTo.text = user?.currencyCode
        self.labelBalAfter.text = "BALANCE AFTER: 0 DIA"
    }
    
    @IBAction func transfer(_ sender: UIButton) {
        if let myVal = labelBalance1.text?.double {
            if !(Double(labelBalance1.text ?? "0")! > 0) {
                self.showAlert(title: "Error", message: "Enter Amount", action1Name: "Ok", action2Name: nil)
            } else {
                showPopUp(view: finalPreviewPopUp, fundTransfer: nil)
            }
        }else {
            self.labelBalance1.text = "0"
            self.labelBalance2.text = "0"
        }
    }
    
    @IBAction func proceed(_ sender: UIButton) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlePasscode), name: NSNotification.Name(N_ENTER_PASSCODE), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.presentPasscodeScreen), name: NSNotification.Name(N_PRESENT_PASSCODE), object: nil)
        passCodeScreen = "AmountScreen"
        Singleton.shared.authenticationWithTouchID(cancel: true, completionHandler: {
            DispatchQueue.main.async {
                self.hidePopUp(view: self.finalPreviewPopUp, popBack: false)
            }
            self.apiCallToTransfer()
        })
    }
    
    @objc func handlePasscode() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_ENTER_PASSCODE), object: nil)
        DispatchQueue.main.async {
            self.hidePopUp(view: self.finalPreviewPopUp, popBack: false)
        }
        self.apiCallToTransfer()
    }
    
    @objc func presentPasscodeScreen() {
        passCodeScreen = "AmountScreen"
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_PRESENT_PASSCODE), object: nil)
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "EnterPasscodeViewController") as! EnterPasscodeViewController
        self.navigationController?.pushViewController(myVC, animated: false)
    }
    
    @IBAction func closePopUp(_ sender: UIButton) {
        hidePopUp(view: transferConfirmPopUp, popBack: true)
    }
    
    @IBAction func back(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
