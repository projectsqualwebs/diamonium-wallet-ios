//
//  SettingTableViewCell.swift
//  Diamonium
//
//

import UIKit

class SettingTableViewCell: UITableViewCell {

    //MARK: IBOutlet
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var settingSwitch: UISwitch!
    @IBOutlet weak var checkMark: UIImageView!
    @IBOutlet weak var labelRightText: UILabel!
    
    var valueChanged: (() -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK: Action
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        if let valueChanged = valueChanged {
            valueChanged()
        }
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
