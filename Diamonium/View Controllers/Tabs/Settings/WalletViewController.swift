//
//  WalletViewController.swift
//  Diamonium
//
//

import UIKit

class WalletViewController: UIViewController {
   //MARK: IBOutlets
    @IBOutlet weak var userTable: UITableView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.userTable.tableFooterView = UIView()
    }
    
    //MARK: Actions
    @IBAction func back(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addAccounts(_ sender: Any) {
        let splashController = storyboard?.instantiateViewController(withIdentifier: "SplashScreenViewController") as! SplashScreenViewController
        splashController.showCancel = true
        SinglePageViewController.controller = K_SPLASH
        self.puchController(controller: splashController)
    }
}

extension WalletViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DBManager.sharedInstance.getUsersFromDB().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WalletTableViewCell", for: indexPath) as! WalletTableViewCell
        
        let user = (DBManager.sharedInstance.getUsersFromDB())[indexPath.row]
        cell.labelUsername.text = user.accNumber
        cell.labelAccType.text = user.accType
        if let image = user.profileImage, image != "" {
            cell.profileImage.sd_setImage(with: URL(string: user.profileImage!)!, placeholderImage: #imageLiteral(resourceName: "defaultProfile.png"))
        }
        
        if user.token == Singleton.shared.userToken {
            cell.imageCheck.isHidden = false
        } else {
            cell.imageCheck.isHidden = true
        }
        
        return cell
    }
}
extension WalletViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let user = (DBManager.sharedInstance.getUsersFromDB())[indexPath.row]
        UserDefaults.standard.set(user.token, forKey: K_TOKEN)
        
        self.popToTabController()
        NotificationCenter.default.post(name: NSNotification.Name("updateUserData"), object: nil)
//        self.view.window?.rootViewController?.dismiss(animated: true, completion: {
//
//        })
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
