//
//  NotificationViewController.swift
//  Diamonium
//
//  Created by AM on 01/05/19.
//

import UIKit

class NotificationViewController: UIViewController {
    //IBOUTLETS
    
    @IBOutlet weak var NewsTable: UITableView!
    
    var arrayNewsFeed: [Feeds]?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        apiCallToGetNewsFeeds()
    }
    
    func apiCallToGetNewsFeeds() {
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE+U_GET_FEEDS, method: .get, parameter: nil, objectClass: FeedsResponse.self, requestCode: U_GET_FEEDS, userToken:nil) { (response) in
            ActivityIndicator.hide()
            self.arrayNewsFeed = [Feeds]()
            for index in 0..<(response.feeds?.count)! {
                let newsFeed = response.feeds?[index]
                //                if self.closedFeeds.count > 0 {
                //                    if newsFeed?.id?.description != self.closedFeeds[index].id {
                //                        self.arrayNewsFeed?.append(Feeds(newsFeeds: newsFeed, readMore: false))
                //                    }
                //                } else {
                self.arrayNewsFeed?.append(Feeds(newsFeeds: newsFeed, readMore: false))
                //  }
            }
            self.NewsTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any){
  self.navigationController?.popViewController(animated: true)
    }
    
}

extension NotificationViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayNewsFeed?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell", for: indexPath) as! NotificationTableViewCell
        let newsFeed = arrayNewsFeed![indexPath.row]
        
        cell.labelHeadline.text = newsFeed.newsFeeds?.title
        cell.labelContent.text = newsFeed.newsFeeds?.description
        cell.labelDate.text = convertTimestampToDate(newsFeed.newsFeeds?.created_timestamp ?? 0, to: "MMMM dd, YYYY")
        
        cell.readMore = {
            self.openUrl(urlStr: (newsFeed.newsFeeds?.url)!)
        }
        
        return cell
    }
}
extension NotificationViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let newFeed = arrayNewsFeed![indexPath.row]
        //        if newFeed.readMore {
        return UITableView.automaticDimension
        //        } else {
        //            return 180
        //        }
    }
}

class NotificationTableViewCell: UITableViewCell {
    
    //MARK: IBOutlet
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelHeadline: UILabel!
    @IBOutlet weak var labelContent: UILabel!
    @IBOutlet weak var buttonMoreInfo: UIButton!
    
    var readMore: (() -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func moreInfo(_ sender: UIButton) {
        if let readMore = readMore {
            readMore()
        }
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

