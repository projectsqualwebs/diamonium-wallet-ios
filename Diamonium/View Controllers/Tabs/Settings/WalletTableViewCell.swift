//
//  WalletTableViewCell.swift
//  Diamonium
//
//

import UIKit

class WalletTableViewCell: UITableViewCell {

    //MARK: IBOutlet
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var labelAccType: UILabel!
    @IBOutlet weak var imageCheck: UIImageView!
    @IBOutlet weak var profileImage: ImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
