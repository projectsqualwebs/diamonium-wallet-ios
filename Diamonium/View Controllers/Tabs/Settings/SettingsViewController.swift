//
//  SettingsViewController.swift
//  Diamonium
//
//

import UIKit
import LocalAuthentication

class SettingsViewController: UIViewController {
    
    //MARK: IBOutlet
    @IBOutlet weak var tableViewSettings: UITableView!
    @IBOutlet weak var privateKeyPopUp: UIView!
    @IBOutlet weak var contentView: View!
    @IBOutlet weak var labePrivateKey: UILabel!
    @IBOutlet weak var clipboard: View!
    
    var arrayContent: [SettingsHeading] = [SettingsHeading]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .black
        
        arrayContent.append(SettingsHeading(title: "", content: [Settings(icon: #imageLiteral(resourceName: "wallet"), name: "Accounts"),Settings(icon: #imageLiteral(resourceName: "logout(2)"), name: "Logout")]))
        
        var security = [Settings(icon: #imageLiteral(resourceName: "password"), name: "Passcode / Touch ID")]
        if Singleton.shared.enableAuthentication {
            security.append(Settings(icon: #imageLiteral(resourceName: "settings-1"), name: "Advanced Settings"))
        }
        arrayContent.append(SettingsHeading(title: "Security", content: security))
        
        let currency = [Settings(icon: #imageLiteral(resourceName: "currency"), name: "Currency"), Settings(icon: #imageLiteral(resourceName: "transaction"), name: "Transactions")
                        //Settings(icon: #imageLiteral(resourceName: "ic_notification"), name: "Notifications")
                        , Settings(icon: #imageLiteral(resourceName: "password"), name: "Private Key")]
        arrayContent.append(SettingsHeading(title: "", content: currency))
        //Settings(icon: #imageLiteral(resourceName: "transaction"), name: "Swap"),
        getSocialFromDB()
    }
    
    func getSocialFromDB() {
        var community = [Settings]()
        let socialDB = DBManager.sharedInstance.getSocialFromDB()
        if socialDB.count > 0 {
            for social in socialDB {
                community.append(Settings(icon: UIImage(named: "\(social.imageName ?? "").png")!, name: social.name ?? "", url: social.url))
            }
            self.arrayContent.append(SettingsHeading(title: "Join community", content: community))
            self.tableViewSettings.reloadData()
        }
    }
    
    func showPopUp() {
        privateKeyPopUp.isHidden = false
        labePrivateKey.text = DBManager.sharedInstance.currentUser[0].key
        
        privateKeyPopUp.alpha = 0.3
        contentView.alpha = 0.5
        contentView.transform = CGAffineTransform(scaleX: 0.4, y: 0.4)
        
        UIView.animate(withDuration: 0.5) {
            self.privateKeyPopUp.alpha = 1
            self.contentView.alpha = 1
            self.contentView.transform = CGAffineTransform(scaleX: 1, y: 1)
        }
    }
    
    func hidePopUp() {
        UIView.animate(withDuration: 0.5, animations: {
            self.privateKeyPopUp.alpha = 0
            self.contentView.alpha = 0
            self.contentView.transform = CGAffineTransform(scaleX: 0.4, y: 0.4)
        }) { (_) in
        }
    }
    
    //MARK: Actions
    @IBAction func closePopUp(_ sender: UIButton) {
        hidePopUp()
    }
    
    @IBAction func copyText(_ sender: UIButton) {
        self.copyText(object: clipboard, text: labePrivateKey.text)
    }
    
    @IBAction func back(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
extension SettingsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayContent.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayContent[section].content!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingTableViewCell", for: indexPath) as! SettingTableViewCell
        
        let content = arrayContent[indexPath.section].content![indexPath.row]
        cell.icon.image = content.icon
        cell.labelText.text = content.name
        
        if content.name == "Passcode / Touch ID" {
            cell.labelRightText.isHidden = true
            cell.settingSwitch.isHidden = false
            cell.settingSwitch.setOn(Singleton.shared.enableAuthentication, animated: false)
        } else if content.name == "Currency" {
            cell.labelRightText.text = Singleton.shared.selectedCurrency.description
            cell.labelRightText.isHidden = false
            cell.settingSwitch.isHidden = true
        } else if content.name == "Accounts" {
            cell.labelRightText.text = DBManager.sharedInstance.currentUser[0].accNumber
            cell.labelRightText.isHidden = false
            cell.settingSwitch.isHidden = true
        } else {
            cell.labelRightText.isHidden = true
            cell.settingSwitch.isHidden = true
        }
        
        cell.valueChanged = {
            if cell.settingSwitch.isOn {
                Singleton.shared.enableAuthentication = true
                self.arrayContent[indexPath.section].content?.append(Settings(icon: #imageLiteral(resourceName: "settings-1"), name: "Advanced Settings"))
                tableView.insertRows(at: [IndexPath(row: 1, section: indexPath.section)], with: .automatic)
                Singleton.shared.autoLockTimer = AutoLock.immediate.interval
            } else {
                Singleton.shared.enableAuthentication = false
                self.arrayContent[indexPath.section].content?.remove(at: 1)
                tableView.deleteRows(at: [IndexPath(row: 1, section: indexPath.section)], with: .automatic)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return arrayContent[section].title
    }
}
extension SettingsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let content = arrayContent[indexPath.section].content![indexPath.row]
        switch content.name {
        case "Accounts":
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
            puchController(controller: VC)
            break
        case "Logout":
            self.showPopup(title: "Logout", msg: "Are you sure?")
        break
        case "Passcode / Touch ID":
            break
        case "Private Key":
            self.showPopUp()
            break
        case "Advanced Settings":
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "SubSettingContentViewController") as! SubSettingContentViewController
            
            VC.navBarTitle = content.name
            VC.arrayContent = [SettingsHeading(title: "Advanced Settings", content: [Settings(icon: #imageLiteral(resourceName: "defaultProfile.png"), name: "Auto-Lock")])]
            
            puchController(controller: VC)
            break
        case "Currency":
            var currencyContent: [Currency] = []
            
            for code in NSLocale.isoCurrencyCodes as [String] {
                let locale = NSLocale(localeIdentifier: Locale.current.identifier  )
                if let name = locale.displayName(forKey: .currencyCode, value: code), let symbol = locale.displayName(forKey: .currencySymbol, value: code), !(name.contains("(")) {
                    currencyContent.append(Currency(description: "\(code) - \(name)", code: code, symbol: symbol, isSelected: false))
                }
            }
            
            print(currencyContent)
            
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "SubSettingContentViewController") as! SubSettingContentViewController
            
            VC.navBarTitle = content.name
            VC.currencyList = currencyContent
            
            puchController(controller: VC)
            break
        case "Transactions":
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "TransactionViewController") as! TransactionViewController
            puchController(controller: VC)
            break
//        case "Swap":
//            let VC = self.storyboard?.instantiateViewController(withIdentifier: "TransactionViewController") as! TransactionViewController
//            puchController(controller: VC)
//            break
        case "Notifications":
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            puchController(controller: VC)
            break
        case "Twitter":
            openUrl(urlStr: content.url!)
            //            openUrl(urlStr: "https://twitter.com/Barteos_org")
            break
        case "Linkedin":
            openUrl(urlStr: content.url!)
            //            openUrl(urlStr: "https://au.linkedin.com/company/barteos")
            break
        case "Facebook":
            openUrl(urlStr: content.url!)
            //            openUrl(urlStr: "https://www.facebook.com/barteos.org/")
            break
        default:
            print("default")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func showPopup(title: String, msg: String) {
           let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
           let yesBtn = UIAlertAction(title:"Yes", style: .default) { (UIAlertAction) in
            UserDefaults.standard.removeObject(forKey: K_TOKEN)
            UserDefaults.standard.removeObject(forKey: K_BALANCE)
            UserDefaults.standard.removeObject(forKey: K_TOKEN_BALANCE)
            DBManager.sharedInstance.deleteAllFromDB()
            let splashController = self.storyboard?.instantiateViewController(withIdentifier: "SplashScreenViewController") as! SplashScreenViewController
            splashController.showCancel = false
            SinglePageViewController.controller = K_SPLASH
            self.puchController(controller: splashController)
        }
           let noBtn = UIAlertAction(title: "No", style: .default){
               (UIAlertAction) in
               self.dismiss(animated: true, completion: nil)
           }
           alert.addAction(yesBtn)
           alert.addAction(noBtn)
           present(alert, animated: true, completion: nil)
       }
}

