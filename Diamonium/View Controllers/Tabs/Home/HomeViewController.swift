//
//  HomeViewController.swift
//  Diamonium
//
//

import UIKit
import RealmSwift
//import Crashlytics

class HomeViewController: UIViewController {
    
    //MARK: IBOutlet
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var labelAccountType: UILabel!
    @IBOutlet weak var labelAccountNumber: UILabel!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var labelDiamBalance: UILabel!
    @IBOutlet weak var labelTokenBalance: UILabel!
    @IBOutlet weak var labelBalanceAccToCurrency: UILabel!
    @IBOutlet weak var tableViewFeeds: UITableView!
    
    var arrayNewsFeed = [Feeds]()
    let formatter = NumberFormatter()
    var userToken: String?
    var user: DBUsers?
    
    let closedFeeds = DBManager.sharedInstance.getClosedFeedsFromDB()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userToken = user?.token
        formatter.groupingSize = 3
        formatter.secondaryGroupingSize = 3
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        
        updateUserData()
        //updateBalance()
        
        NotificationCenter.default.addObserver(self, selector: #selector(apiCallToGetDiamBalance), name: NSNotification.Name("updateCurrency"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateBalance), name: NSNotification.Name("updateBalance"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUserData), name: NSNotification.Name("updateUserData"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let info = UserDefaults.standard.value(forKey: "notificationInfo") {
            UserDefaults.standard.removeObject(forKey: "notificationInfo")
           // UserDefaults.standard.set("Show loader", forKey: "transUpdate")
            self.apiCallToGetDiamBalance()
            print(info)
        }else {
            print("ok")
            
        }
    }
    
    @objc func updateBalance() {
        labelBalanceAccToCurrency.text = "\(Singleton.shared.selectedCurrency.code) BALANCE"
        
        labelDiamBalance.text = (self.formatter.string(from: (Double(Singleton.shared.balance) ?? 0) as NSNumber)!)
        //        (Singleton.shared.selectedCurrency.symbol) + " " +
        labelTokenBalance.text = (self.formatter.string(from: (Double(Singleton.shared.tokenBalance) ?? 0) as! NSNumber)!)
    }
    
    @objc func updateUserData() {
        let pageVC = PageViewController.dataSource1 as? PageViewController
        pageVC?.setControllerFirst()
        PageViewController.index_delegate?.getControllerIndex(index: 0)
        
        labelUsername.text = DBManager.sharedInstance.currentUser[0].name ?? "Not Available"
        labelAccountType.text = DBManager.sharedInstance.currentUser[0].accType ?? "Individual"
        labelAccountNumber.text = DBManager.sharedInstance.currentUser[0].accNumber
        if let image = DBManager.sharedInstance.currentUser[0].profileImage, image != "" {
            userImage.sd_setImage(with: URL(string: image)!, placeholderImage: #imageLiteral(resourceName: "defaultProfile.png"))
        } else {
            userImage.image = #imageLiteral(resourceName: "defaultProfile.png")
        }
        
        updateBalance()
        self.apiCallToGetDiamBalance()
        apiCallToGetNewsFeeds()
        updateLastTransaction()
    }
    
    func updateLastTransaction() {
        if let transactionId = DBManager.sharedInstance.getLastTransactionId(){
            if(transactionId != nil && transactionId != ""){
                Router.getTransactionHistory(transId: transactionId)
            }
        }
    }
    
    @objc func apiCallToGetDiamBalance() {
        labelBalanceAccToCurrency.text = "\(Singleton.shared.selectedCurrency.code) BALANCE"
        self.labelDiamBalance.text =  0.description
        SessionManager.shared.apiCallToGetBalance { (balance, tokenBalance) in
            self.labelDiamBalance.text = (self.formatter.string(from: (Double(balance) ?? 0) as! NSNumber)!)
            //            (Singleton.shared.selectedCurrency.symbol) + " " +
            self.labelTokenBalance.text = (self.formatter.string(from: (Double(tokenBalance) ?? 0) as! NSNumber)!)
            DispatchQueue.main.async {
                DBManager.sharedInstance.updateUserData(name: nil, email: nil, imagePath: nil, currency: nil, bteBalance: tokenBalance, currencyBalance: balance)
            }
        }
    }
    
    func apiCallToGetNewsFeeds() {
        SessionManager.shared.methodForApiCalling(url: U_BASE+U_GET_FEEDS, method: .get, parameter: nil, objectClass: FeedsResponse.self, requestCode: U_GET_FEEDS, userToken:nil) { (response) in
            self.arrayNewsFeed = [Feeds]()
            for index in 0..<(response.feeds?.count)! {
                let newsFeed = response.feeds?[index]
                var valueExist = false
                if (self.closedFeeds.count > 0) {
                for feedIndex in self.closedFeeds {
                    if(newsFeed?.id?.description == feedIndex.id){
                        valueExist = true
                    }
                }
                    if(!valueExist){
                        self.arrayNewsFeed.append(Feeds(newsFeeds: newsFeed, readMore: false))
                        valueExist = false
                    }
                }else {
                    self.arrayNewsFeed.append(Feeds(newsFeeds: newsFeed, readMore: false))
                }
//
//
//                if (self.closedFeeds.count > 0) {
//                    if(index < self.closedFeeds.count - 1){
//                        if newsFeed?.id?.description != self.closedFeeds[index].id {
//                            self.arrayNewsFeed.append(Feeds(newsFeeds: newsFeed, readMore: false))
//                        }
//                    }
//                } else {
//                    self.arrayNewsFeed.append(Feeds(newsFeeds: newsFeed, readMore: false))
//                }
            }
            self.tableViewFeeds.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    func attributedString(_ value: String) -> NSMutableAttributedString {
        let attrValue = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
        let attrValue2 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 25)]
        let newValue = value.components(separatedBy: ".")
        let beforeDecimal = newValue[0]
        let afterDecimal = newValue[1]
        var newSTring = NSMutableAttributedString(string: "\(beforeDecimal).", attributes: attrValue2)
        newSTring.append(NSAttributedString(string: afterDecimal, attributes: attrValue))
        return newSTring
    }
    
    
    //MARK: Action
    @IBAction func showProfile(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        controller.updateProfileDelegate = self
        
        self.puchController(controller: controller)
    }
    
    @IBAction func buyDiam(_ sender: UIButton) {
        let buyController = self.storyboard?.instantiateViewController(withIdentifier: "BuyDiamViewController") as! BuyDiamViewController
        navigationController?.pushViewController(buyController, animated: true)
    }
    
    //    @IBAction func more(_ sender: UIButton) {
    //        if labelContent.numberOfLines == 0 {
    //            labelContent.numberOfLines = 4
    //            sender.setTitle("More Info", for: .normal)
    //        } else {
    //            sender.setTitle("Less Info", for: .normal)
    //            labelContent.numberOfLines = 0
    //        }
    //    }
}

extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayNewsFeed.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsFeedsTableViewCell", for: indexPath) as! NewsFeedsTableViewCell
        let newsFeed = arrayNewsFeed[indexPath.row]
        
        cell.labelHeadline.text = newsFeed.newsFeeds?.title
        cell.labelContent.text = newsFeed.newsFeeds?.description
        cell.labelDate.text = convertTimestampToDate(newsFeed.newsFeeds?.created_timestamp ?? 0, to: "MMMM dd, YYYY")
        
        cell.readMore = {
            self.openUrl(urlStr: (newsFeed.newsFeeds?.url)!)
        }
        
        cell.close = {
            self.arrayNewsFeed.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            DBManager.sharedInstance.addClosedFeeds(newsFeed: newsFeed.newsFeeds)
            self.tableViewFeeds.reloadData()
        }
        
        return cell
    }
}
extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let newFeed = arrayNewsFeed[indexPath.row]
        //        if newFeed.readMore {
        return UITableView.automaticDimension
        //        } else {
        //            return 180
        //        }
    }
}
extension HomeViewController: UpdateProfileDelegate {
    func updateProfile(name: String?, profileImage: UIImage?) {
        userImage.image = profileImage
        labelUsername.text = name
    }
}

