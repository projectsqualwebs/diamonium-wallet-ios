//
//  NewsFeedsTableViewCell.swift
//  Diamonium
//
//

import UIKit

class NewsFeedsTableViewCell: UITableViewCell {

    //MARK: IBOutlet
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelHeadline: UILabel!
    @IBOutlet weak var labelContent: UILabel!
    @IBOutlet weak var buttonMoreInfo: UIButton!
    
    var readMore: (() -> Void)? = nil
    var close: (() -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func moreInfo(_ sender: UIButton) {
        if let readMore = readMore {
            readMore()
        }
    }

    @IBAction func close(_ sender: UIButton) {
        if let close = close {
            close()
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
