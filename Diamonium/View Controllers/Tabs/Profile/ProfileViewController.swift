//
//  ProfileViewController.swift
//  Diamonium
//
//

import UIKit
import SkyFloatingLabelTextField
import CropViewController

class ProfileViewController: UIViewController {
    
    //MARK: IBOutlet
    @IBOutlet weak var userProfile: ImageView!
    @IBOutlet weak var textFieldFullName: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldAccType: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldAccNumber: SkyFloatingLabelTextField!
    
    var updateProfileDelegate: UpdateProfileDelegate? = nil
    let picker = UIImagePickerController()
    var imageName: String = ""
    var imagePath: String?
    let RISTRICTED_CHARACTERS = "'*=+[]\\|;:'\",<>/?%"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldFullName.delegate = self
        picker.delegate = self
        self.userProfile.layer.cornerRadius = self.userProfile.frame.width/2
        self.userProfile.clipsToBounds = true
        self.userProfile.layer.masksToBounds = true
        self.userProfile.contentMode = .scaleAspectFill
        textFieldFullName.text = DBManager.sharedInstance.currentUser[0].name
        textFieldEmail.text = DBManager.sharedInstance.currentUser[0].email
        textFieldAccType.text = DBManager.sharedInstance.currentUser[0].accType
        textFieldAccNumber.text = DBManager.sharedInstance.currentUser[0].accNumber
        if let image = DBManager.sharedInstance.currentUser[0].profileImage, image != "" {
            imagePath = DBManager.sharedInstance.currentUser[0].profileImage
            self.userProfile.sd_setImage(with: URL(string: DBManager.sharedInstance.currentUser[0].profileImage ?? "")!, placeholderImage: #imageLiteral(resourceName: "defaultProfile.png"))
        }

        self.perform(#selector(setImageInCircle(image:)), with: userProfile, afterDelay: 0)
    }
    
    @objc func setImageInCircle(image: UIImageView) {
        image.layer.cornerRadius = image.frame.width/2
        image.clipsToBounds = true
    }
    
    //MARK: Action
    @IBAction func selectProfile(_ sender: UIButton) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
               alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                   self.openCamera()
               }))
               
               alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                   self.openGallary()
               }))
               
               alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
               if let popoverController = alert.popoverPresentationController {
                   popoverController.sourceView = sender as! UIView
                   popoverController.sourceRect = (sender as AnyObject).bounds
               }
               self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func saveProfile(_ sender: Any) {
        if (textFieldEmail.text?.isEmpty)! {
            showAlert(title: "Error", message: "Enter email", action1Name: "Ok", action2Name: nil)
        }else if(self.textFieldFullName.text!.count < 3){
            self.showAlert(title: "Error", message: "Full Name should be greater than 2 characters", action1Name: "Ok", action2Name: nil)
        }else if(self.textFieldFullName.text!.count > 20){
            self.showAlert(title: "Error", message: "Full Name should be less than 20 characters", action1Name: "Ok", action2Name: nil)
        }else if(!isValidEmail(testStr: textFieldEmail.text!)){
            showAlert(title: "Error", message: "Enter valid email", action1Name: "Ok", action2Name: nil)
        } else {
            let param = [K_FULL_NAME: textFieldFullName.text!,
                         K_PROFILE_IMAGE: imagePath ?? "",
                         K_USERNAME: textFieldAccNumber.text!,
                         K_EMAIL: textFieldEmail.text!]
            SessionManager.shared.methodForApiCalling(url: U_BASE2 + U_PROFILE_UPDATE, method: .post, parameter: param, objectClass: Response.self, requestCode: U_PROFILE_UPDATE, userToken: nil) { (response) in
                 self.showAlert(title: "Successfully Updated Profile", message: nil, action1Name: "Ok", action2Name: nil)
                self.updateProfileDelegate?.updateProfile(name: self.textFieldFullName.text, profileImage: self.userProfile.image)
                DBManager.sharedInstance.updateUserData(name: self.textFieldFullName.text, email: self.textFieldEmail.text, imagePath: self.imagePath, currency: nil, bteBalance: nil, currencyBalance: nil)
                self.navigationController?.popViewController(animated: true)
                ActivityIndicator.hide()
            }
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    @IBAction func dismissController(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}

extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        picker.allowsEditing = true
        self.present(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
        
        if let selectedImageName = ((info[UIImagePickerController.InfoKey.referenceURL] as? NSURL)?.lastPathComponent) {
            self.imageName = selectedImageName
        } else {
            self.imageName = "image.jpg"
        }
        
        if let cropImage = selectedImage as? UIImage {
                   let data = cropImage.jpegData(compressionQuality: 0.2) as Data?
                   
                   let paramName = "image"
                   
                   // HTTP Request for upload Picture
                   ActivityIndicator.show(view: self.view)
                   SessionManager.shared.makeMultipartRequest(url: U_BASE2 + U_UPLOAD_WALLET_IMAGE, fileData: data!, param: paramName, fileName: imageName) { (path) in
                       self.userProfile.image = cropImage
                       self.imagePath = path
                   }
               }
         picker.dismiss(animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
extension ProfileViewController: CropViewControllerDelegate {
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        cropViewController.dismiss(animated: true, completion: nil)
    }
}

extension ProfileViewController: UITextFieldDelegate {
      func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
}
