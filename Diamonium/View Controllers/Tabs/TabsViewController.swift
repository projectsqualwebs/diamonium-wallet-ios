//
//  TabViewController.swift
//  Diamonium
//
//

import UIKit
import FirebaseRemoteConfig
//import Crashlytics

class TabsViewController: UIViewController {

    //MARK: IBOutlet
    @IBOutlet weak var imageViewHome: UIImageView!
    @IBOutlet weak var buttonHome: UIButton!
    @IBOutlet weak var imageViewReceive: UIImageView!
    @IBOutlet weak var buttonReceive: UIButton!
    @IBOutlet weak var imageViewSend: UIImageView!
    @IBOutlet weak var buttonSend: UIButton!
    @IBOutlet weak var imageViewSwap: UIImageView!
    @IBOutlet weak var buttonSwap: UIButton!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var navBarHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var navBarView: UIView!
    @IBOutlet weak var tabBarView: UIView!
    @IBOutlet weak var bottomBarHeightConstraint: NSLayoutConstraint!
    
    var imageView: [UIImageView]?
    var button: [UIButton]?
    
    //Remote Config
    var remoteConfig: RemoteConfig!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        remoteConfig = RemoteConfig.remoteConfig()
        let remoteConfigSettings = RemoteConfigSettings()
        remoteConfigSettings.minimumFetchInterval = 0
        remoteConfig.configSettings = remoteConfigSettings
        remoteConfig.setDefaults(fromPlist: "firebaseConfigInfo")
        fetchConfig()
        
        imageView = [imageViewHome, imageViewReceive, imageViewSend, imageViewSwap]
        button = [buttonHome, buttonReceive, buttonSend, buttonSwap]
        
        view.backgroundColor = .black
        navBarView.backgroundColor = .black
        tabBarView.backgroundColor = .black
        
        showSelectedTab(0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
//        setNavigationBar(false)
//        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "smallLogo"), style: .bordered, target: self, action: nil)
//        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "settings.png"), style: .bordered, target: self, action: #selector(settings))
        PageViewController.index_delegate = self
    }
    
//    func setTitle(index: Int) {
//        switch index {
//        case 0:
//            labelTitle.text = "Home"
//            break
//        case 1:
//            labelTitle.text = "Receive"
//            break
//        case 2:
//            labelTitle.text = "Send"
//            break
//        case 3:
//            labelTitle.text = "Settings"
//            break
//        default:
//            print("default")
//        }
//    }
    
    func showSelectedTab(_ selectedIndex: Int) {
//        setTitle(index: selectedIndex)
        for index in 0..<(imageView?.count)! {
            if index == selectedIndex {
                imageView?[index].image = imageView?[index].image?.withRenderingMode(.alwaysTemplate)
                imageView?[index].tintColor = goldColor
                button?[index].setTitleColor(goldColor, for: .normal)
            } else {
                imageView?[index].image = imageView?[index].image?.withRenderingMode(.alwaysTemplate)
                imageView?[index].tintColor = .white
                button?[index].setTitleColor(.white, for: .normal)
            }
        }
    }
    
    func fetchConfig() {
        var expirationDuration = 3600
        if remoteConfig.configSettings.minimumFetchInterval == 0 {
            expirationDuration = 0
        }
        remoteConfig.fetch(withExpirationDuration: TimeInterval(expirationDuration)) { (status, error) -> Void in
            if status == .success {
                print("Config fetched!")
                self.remoteConfig.activate { changed, error in
                }
                self.display()
            } else {
                print("Config not fetched")
                print("Error \(error!.localizedDescription)")
            }
            
        }
    }
    
    func display() {
        var softUpdate =  "update_soft_ios_diamonium"
        var updateRequired = "force_update_required_ios_diamonium"
        var currentVersion = "force_update_current_version_ios_diamonium"
        var updateUrl = "force_update_store_url_ios_diamonium"
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        if(remoteConfig[updateRequired].stringValue == "true"){
            if(remoteConfig[currentVersion].stringValue == appVersion){
                return
            }else{
                if(remoteConfig[softUpdate].stringValue == "false"){
                    self.showPopup(title: "Update", msg: "New Version is available on App Store",action:2)
                }else{
                    self.showPopup(title: "Update", msg:  "New Version is available on App Store",action: 1)
                }
            }
        }else {
            return
        }
    }
    
    func showPopup(title: String, msg: String,action:Int?) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let yesBtn = UIAlertAction(title:"Update", style: .default) { (UIAlertAction) in
            self.yesButtonAction()
        }
        let noBtn = UIAlertAction(title: "Cancel", style: .default){
            (UIAlertAction) in
            if(action != 1){
                self.forceUpdate()
            }
        }
        
        alert.addAction(yesBtn)
        alert.addAction(noBtn)
        
        present(alert, animated: true, completion: nil)
    }
    
    func yesButtonAction(){
        //self.updateUserToken()
        let string = remoteConfig["force_update_store_url_ios_diamonium"].stringValue!
        let url  = NSURL(string: string)//itms   https
        if UIApplication.shared.canOpenURL(url! as URL) {
            UserDefaults.standard.removeObject(forKey: K_TOKEN)
            UserDefaults.standard.removeObject(forKey: K_BALANCE)
            UserDefaults.standard.removeObject(forKey: K_TOKEN_BALANCE)
            DBManager.sharedInstance.deleteAllFromDB()
            let splashController = self.storyboard?.instantiateViewController(withIdentifier: "SplashScreenViewController") as! SplashScreenViewController
            splashController.showCancel = false
            SinglePageViewController.controller = K_SPLASH
            UIApplication.shared.openURL(url! as URL)
            self.puchController(controller: splashController)
        }
    }
    
    func forceUpdate() {
        exit(0);
    }
    
//    func updateUserToken(){
//        if(DBManager.sharedInstance.currentUser.count > 0){
//            let fcm = UserDefaults.standard.value(forKey: FCM)
//            let param = [
//                "username": DBManager.sharedInstance.currentUser[0].name ?? "",
//                "device_type": 1,
//                "firebase_token":
//            ] as? [String:Any]
//            SessionManager.shared.methodForApiCalling(url: U_BASE2 + U_UPDATE_USER_TOKEN, method: .post, parameter: [], objectClass: <#T##(Decodable & Encodable).Protocol#>, requestCode: <#T##String#>, userToken: <#T##String?#>, completionHandler: <#T##(Decodable & Encodable) -> Void#>)
//            UserDefaults.standard.removeObject(forKey: K_TOKEN)
//            UserDefaults.standard.removeObject(forKey: K_BALANCE)
//            UserDefaults.standard.removeObject(forKey: K_TOKEN_BALANCE)
//            DBManager.sharedInstance.deleteAllFromDB()
//            let splashController = self.storyboard?.instantiateViewController(withIdentifier: "SplashScreenViewController") as! SplashScreenViewController
//            splashController.showCancel = false
//            SinglePageViewController.controller = K_SPLASH
//            self.puchController(controller: splashController)
//        }
//    }
    
    //MARK: Action
    @IBAction func settings(_ sender: UIButton) {
        let settingVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        //Crashlytics.sharedInstance().crash()
        self.navigationController?.pushViewController(settingVC, animated: true)
//        self.present(settingVC, animated: true, completion: nil)
    }
    
    @IBAction func home(_ sender: UIButton) {
        showSelectedTab(0)
        let pageVC = PageViewController.dataSource1 as? PageViewController
        pageVC?.setControllerFirst()
    }
    
    @IBAction func receive(_ sender: UIButton) {
        showSelectedTab(1)
        let pageVC = PageViewController.dataSource1 as? PageViewController
        pageVC?.setControllerSecond()
    }
    
    @IBAction func scan(_ sender: UIButton) {
        let scanContoller = self.storyboard?.instantiateViewController(withIdentifier: "ScanViewController") as! ScanViewController
        self.present(scanContoller, animated: true, completion: nil)
    }
    
    @IBAction func send(_ sender: UIButton) {
        showSelectedTab(2)
        let pageVC = PageViewController.dataSource1 as? PageViewController
        pageVC?.setControllerThird()
    }
    
    @IBAction func swap(_ sender: UIButton) {
        showSelectedTab(3)
        let pageVC = PageViewController.dataSource1 as? PageViewController
        pageVC?.setControllerLast()
    }
}
extension TabsViewController: ControllerIndexDelegate {
    func getControllerIndex(index: Int) {
        showSelectedTab(index)
    }
}
