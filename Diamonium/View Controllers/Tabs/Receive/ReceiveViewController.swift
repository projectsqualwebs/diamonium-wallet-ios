//
//  ReceiveViewController.swift
//  Diamonium
//
//

import UIKit

class ReceiveViewController: UIViewController {

    //MARK: IBOutlet
    @IBOutlet weak var imageQRCode: UIImageView!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var usernameView: UIView!
    @IBOutlet weak var clipboard: View!
    @IBOutlet weak var textFieldBalance1: UITextField!
    @IBOutlet weak var labelBalanceTypeFrom: UILabel!
    @IBOutlet weak var labelBalance2: UILabel!
    @IBOutlet weak var labelBalanceTypeTo: UILabel!
//        @IBOutlet weak var textFieldBeValue: UITextField!
//    @IBOutlet weak var labelCurrencyName: UILabel!
//    @IBOutlet weak var textFieldCurrencyValue: UITextField!
    
    var user = DBManager.sharedInstance.currentUser[0]
//    var from: String?
//    var to: String?
    var showBTE: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        usernameView.layer.borderWidth = 1.0
        usernameView.layer.borderColor = primaryColor.cgColor
         textFieldBalance1.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateBalance), name: NSNotification.Name("updateUserData"), object: nil)
    
        textFieldBalance1.addDoneOnKeyboardWithTarget(self, action: #selector(done(_:)))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        user = DBManager.sharedInstance.currentUser[0]
        labelBalanceTypeFrom.text = user.currencyCode
        labelUsername.text = user.accNumber!
        
        generateQRCodeWithNameAmount(bte: "0", localValue: "0")
    }
    
    @objc func updateBalance() {
        textFieldBalance1.text = "0"
        labelBalance2.text = "0"
    }
    
    func generateQRCodeWithNameAmount(bte: String, localValue: String) {
        let qrValue = (user.accNumber!) + "+" + bte + "+" + localValue
        imageQRCode.image = generateQRCode(from: qrValue)
    }
    
    @objc func done(_ : UIBarButtonItem) {
        textFieldBalance1.resignFirstResponder()
        getAmount(text: textFieldBalance1.text)
    }
    
    func apiCallToConvertCurrency(_ amount: Double) {
        if amount > 0 {
            let param = [K_AMOUNT: amount,
                         K_CURRENCY_FROM: labelBalanceTypeFrom.text!,
                         K_CURRENCY_TO: labelBalanceTypeTo.text!] as [String : Any]
            
            SessionManager.shared.methodForApiCalling(url: U_BASE2+U_CURRENCY_CONVERT, method: .post, parameter: param, objectClass: CurrencyBalances.self, requestCode: U_CURRENCY_CONVERT, userToken: nil) { (balance) in
                
                if !(self.showBTE) {
                    self.labelBalance2.text = String(format: "%.2f", (balance.be_value) ?? 0)
                } else {
                    self.labelBalance2.text = String(format: "%.2f", (balance.local_value) ?? 0)
                }
                
                self.generateQRCodeWithNameAmount(bte: String(format: "%.2f", balance.be_value ?? 0), localValue: String(format: "%.2f", balance.local_value ?? 0))
                ActivityIndicator.hide()
            }
        } else {
            textFieldBalance1.text = 0.description
            self.labelBalance2.text = 0.description
            self.generateQRCodeWithNameAmount(bte: "0", localValue: "0")
        }
    }
    
    func getAmount(text: String?) {
        if let value = text, let validNumber = Double(value) {
            apiCallToConvertCurrency(Double(text ?? "0")!)
        } else {
            textFieldBalance1.text = 0.description
            labelBalance2.text = 0.description
            self.generateQRCodeWithNameAmount(bte: "0", localValue: "0")
        }
    }
    
//    func generateQRCode(from string: String) -> UIImage? {
//        let data = string.data(using: String.Encoding.utf8)
//        if let filter = CIFilter(name: "CIQRCodeGenerator") {
//            guard let colorFilter = CIFilter(name: "CIFalseColor") else { return nil }
//
//            filter.setValue(data, forKey: "inputMessage")
//
////            filter.setValue("H", forKey: "inputCorrectionLevel")
//            colorFilter.setValue(filter.outputImage, forKey: "inputImage")
//            colorFilter.setValue(CIColor(red: 1, green: 1, blue: 1), forKey: "inputColor1") // Background white
//            colorFilter.setValue(CIColor(red: 229/255, green: 178/255, blue: 69/255), forKey: "inputColor0") // Foreground or the barcode RED
//            guard let qrCodeImage = colorFilter.outputImage
//                else {
//                    return nil
//            }
//            let scaleX = imageQRCode.frame.size.width / qrCodeImage.extent.size.width
//            let scaleY = imageQRCode.frame.size.height / qrCodeImage.extent.size.height
//            let transform = CGAffineTransform(scaleX: scaleX, y: scaleY)
//
//            if let output = colorFilter.outputImage?.transformed(by: transform) {
//                return UIImage(ciImage: output)
//            }
//        }
//        return nil
//    }
    
    //MARK: Action
    @IBAction func copyText(_ sender: UIButton) {
        self.copyText(object: clipboard, text: labelUsername.text)
    }
    
    @IBAction func pasteAmount(_ sender: UIButton) {
        if let value = UIPasteboard.general.string, let validNumber = Double(value) {
            self.textFieldBalance1.text = validNumber.description
            self.apiCallToConvertCurrency(validNumber)
        } else {
            self.showAlert(title: "Error", message: "Please paste valid amount", action1Name: "Ok", action2Name: nil)
        }
    }
    
    @IBAction func switchCurrency(_ sender: Any) {
        showBTE = !showBTE
        
        textFieldBalance1.text = "0"
        if showBTE {
            labelBalanceTypeFrom.text = "DIA"
            labelBalance2.text = "0"
            labelBalanceTypeTo.text = user.currencyCode
        } else {
            labelBalanceTypeFrom.text = user.currencyCode
            labelBalance2.text = "0"
            labelBalanceTypeTo.text = "DIA"
        }
        
        generateQRCodeWithNameAmount(bte: "0", localValue: "0")
    }
}
extension ReceiveViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.count == 1) && string == "" {
            getAmount(text: "0")
        } else if string == "" {
            getAmount(text: textField.text?.dropLast().description)
        } else {
            getAmount(text: textField.text!+string)
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.textFieldBalance1){
            self.textFieldBalance1.text = ""
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
