//
//  ScanViewController.swift
//  Diamonium
//
//

import UIKit
import AVFoundation

class ScanViewController: UIViewController {

    //MARK: IBOutlet
    @IBOutlet weak var qrCodeView: UIView!
    @IBOutlet weak var qrFrameView: UIView!
    
    var captureSession = AVCaptureSession()
    var videoPreviewLawyer: AVCaptureVideoPreviewLayer?
    
    var comingFromSend: Bool = false
    var isFirstTime = true

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = barBackgroundColor
        self.goToCamera()
//        if AVCaptureDevice.authorizationStatus(for: .video) == .authorized {
//            
//        }else {
//            AVCaptureDevice.requestAccess(for: .video) { (granted:Bool) in
//                if granted {
//                    
//                }else {
//                    self.dismiss(animated: true, completion: nil)
//                }
//            }
//        }
        
        self.configureVideoCapture()
        self.initializeQRView()
        if !(AVCaptureDevice.authorizationStatus(for: AVMediaType.video) == .denied){
            self.addVideoPreviewLayer()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.startScaning()
        self.isFirstTime = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.isFirstTime = true
    }
 
    
    @IBAction func goToCamera()
    {
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch (status)
        {
        case .authorized:
           // self.goToCamera()
            self.configureVideoCapture()
            self.addVideoPreviewLayer()
            self.startScaning()
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video) { (granted) in
                if (granted)
                {
                    self.configureVideoCapture()
                    self.addVideoPreviewLayer()
                    self.startScaning()
                }
                else
                {
                    self.camDenied()
                }
            }
            
        case .denied:
            self.camDenied()
            
        case .restricted:
            let alert = UIAlertController(title: "Restricted",
                                          message: "You've been restricted from using the camera on this device. Without camera access this feature won't work. Please contact the device owner so they can give you access.",
                                          preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func camDenied()
    {
        DispatchQueue.main.async
            {
                var alertText = "It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following:\n\n1. Close this app.\n\n2. Open the Settings app.\n\n3. Scroll to the bottom and select this app in the list.\n\n4. Turn the Camera on.\n\n5. Open this app and try again."
                
                var alertButton = "Ok"
                var cancelButton = "Cancel"
                var goAction = UIAlertAction(title: alertButton, style: .default, handler: nil)
                goAction = UIAlertAction(title: alertButton, style: .default, handler: {(alert: UIAlertAction!) -> Void in
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
                })
                let cancelAction = UIAlertAction(title: "Cancel", style: .default) {
                    UIAlertAction in
                     self.dismiss(animated: true, completion: nil)
                    NSLog("Cancel Pressed")
                }
                
                if UIApplication.shared.canOpenURL(URL(string: UIApplication.openSettingsURLString)!)
                {
                    alertText = "It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following:\n\n1. Touch the Ok button below to open the Settings app.\n\n2. Turn the Camera on.\n\n3. Open this app and try again."
                    
                   // alertButton = "Go"
                   // alertButton = "Cancel"
                }
                
                let alert = UIAlertController(title: "Error", message: alertText, preferredStyle: .alert)
                
                alert.addAction(cancelAction)
                alert.addAction(goAction)
                self.present(alert, animated: true, completion: nil)
        }
    }
    
//   @objc func cancelAction() {
//
//    }
    
    func configureVideoCapture() {
        // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
        // as the media type parameter.
        if let captureDevice = AVCaptureDevice.default(for: AVMediaType.video){
        
        // Get an instance of the AVCaptureDeviceInput class using the previous device object.
        var error:NSError?
        do {
            let input: AVCaptureInput = try AVCaptureDeviceInput(device: captureDevice)
            // Initialize the captureSession object.
            captureSession = AVCaptureSession()
            // Set the input device on the capture session.
            captureSession.addInput(input)
        } catch let error as NSError {
            // If any error occurs, simply log the description of it and don't continue any more.
            print("\(error.localizedDescription)")
            return
        }
        
        // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
        let captureMetadataOutput = AVCaptureMetadataOutput()
        captureSession.addOutput(captureMetadataOutput)
        // Set delegate and use the default dispatch queue to execute the call back
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        }
    }
    
    // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
    
    func addVideoPreviewLayer() {
      videoPreviewLawyer =  AVCaptureVideoPreviewLayer(session: self.captureSession)
        videoPreviewLawyer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLawyer?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height-64)
        qrCodeView.layer.addSublayer(videoPreviewLawyer!)
        self.view.bringSubviewToFront(qrFrameView)
        
    }
    
    func initializeQRView() {
        qrFrameView.layer.borderColor = primaryColor.cgColor
        qrFrameView.layer.borderWidth = 2.0
    }
    
    // Start video capture.
    func startScaning() {
        captureSession.startRunning()
    }
    
    func stopScaning() {
        captureSession.stopRunning()
        captureSession = AVCaptureSession()
        videoPreviewLawyer?.removeFromSuperlayer()
    }
    
    //MARK: Action
    @IBAction func back(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
extension ScanViewController: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        // Check if the metadataObjects array is not nil and it contains at least one object.
        
        if metadataObjects == nil || metadataObjects.count == 0 {
            self.showAlert(title: "Error", message: "No QR code is detected", action1Name: "Ok", action2Name: nil)
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if metadataObj.type == AVMetadataObject.ObjectType.qr {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLawyer?.transformedMetadataObject(for: metadataObj)
            if let value = metadataObj.stringValue, value.matches(QRCODE_REGEX) {
                stopScaning()
                dismiss(animated: true) {
                    if !(self.comingFromSend) {
                        let pageVC = PageViewController.dataSource1 as? PageViewController
                        pageVC?.setControllerThird()
                        PageViewController.index_delegate?.getControllerIndex(index: 2)
                    }
                    if(self.isFirstTime){
                        self.isFirstTime = false
                    NotificationCenter.default.post(name: NSNotification.Name("ScanQRCode"), object: value)
                    }
                }
                self.isFirstTime = true
            }
        }
    }
}
