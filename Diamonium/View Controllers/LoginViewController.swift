//
//  LoginViewController.swift
//  Diamonium
//


import UIKit
import SkyFloatingLabelTextField
//import Crashlytics
import FlagPhoneNumber
import IQKeyboardManagerSwift

class LoginViewController: UIViewController {

    //MARK: IBOutlet
   // @IBOutlet weak var textFieldUsername: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldMobile: FPNTextField!
    @IBOutlet weak var labelMobilePrefix: UILabel!
     //@IBOutlet weak var labelUsernamePrefix: UILabel!
    @IBOutlet weak var textFieldPassword: SkyFloatingLabelTextField!
    
     var countryCode: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textFieldMobile.attributedPlaceholder = NSAttributedString(string:"Username", attributes:[NSAttributedString.Key.foregroundColor: UIColor.white])
        self.textFieldPassword.attributedPlaceholder = NSAttributedString(string:self.textFieldPassword.placeholder!, attributes:[NSAttributedString.Key.foregroundColor: UIColor.white])
        IQKeyboardManager.shared.enable = true
        textFieldMobile.font = UIFont.systemFont(ofSize: 14)
        textFieldMobile.delegate = self
        labelMobilePrefix.text = Locale.current.currencyCode
        
        countryCode = textFieldMobile.selectedCountry?.phoneCode.replacingOccurrences(of: "+", with: "")
        textFieldMobile.textColor = .white
        textFieldMobile.enableMode = .enabled
    }
    
    //MARK: Action
    @IBAction func showPassword(_ sender: UIButton) {
        if textFieldPassword.isSecureTextEntry {
            textFieldPassword.isSecureTextEntry = false
        } else {
            textFieldPassword.isSecureTextEntry = true
        }
    }
    
    @IBAction func resetPassword(_ sender: UIButton) {
        let loginController = storyboard?.instantiateViewController(withIdentifier: "RestoreWalletViewController") as! RestoreWalletViewController
        navigationController?.pushViewController(loginController, animated: true)
    }
    
    @IBAction func login(_ sender: UIButton) {
          let mobilenumber = ((labelMobilePrefix.text)?.lowercased())! + "-" + (countryCode ?? "") + textFieldMobile.text!.replacingOccurrences(of: " ", with: "")
        if (textFieldMobile.text?.isEmpty)! {
            showAlert(title: "Required", message: "Enter Username", action1Name: "Ok", action2Name: nil)
        } else if (textFieldPassword.text?.isEmpty)! {
            showAlert(title: "Required", message: "Enter Password", action1Name: "Ok", action2Name: nil)
        } else if (textFieldPassword.text?.characters.count)! < 6 {
            showAlert(title: "Error", message: "Enter valid password", action1Name: "Ok", action2Name: nil)
        } else if DBManager.sharedInstance.userAvailability(accNumber:mobilenumber) {
            showAlert(title: "Error", message: "You're already logged in with this user.", action1Name: "Ok", action2Name: nil)
        } else {
            ActivityIndicator.show(view: self.view)
          
            let param = [K_USERNAME: mobilenumber,
                         K_PASSWORD: textFieldPassword.text!,
                         K_FIREBASE_TOKEN: Singleton.shared.firebaseToken,
                         K_DEVICE_TYPE: 1] as [String : Any]
            SessionManager.shared.methodForApiCalling(url: U_BASE2+U_LOGIN, method: .post, parameter: param, objectClass: LoginResponse.self, requestCode: U_REGISTER, userToken: nil) { response in
                
                UserDefaults.standard.set(response.token, forKey: K_TOKEN)
                Singleton.shared.enableAuthentication = true
                
                var phone: String?
                if let username = response.data?[0].name?.components(separatedBy: "-"), username.count > 1 {
                    phone = username[1]
                }
                
                DBManager.sharedInstance.addUser(name: response.data?[0].full_name, email: response.data?[0].email, phone: phone, token: response.token, key: response.key, accType: response.data?[0].acc_type, accNumber:mobilenumber, profileImage: response.data?[0].profile_image, bteBalance: "0", currencyBalance: "0")
                
                SessionManager.shared.apiCallToGetBalance(completionHandler: { (balance, tokenBalance) in
                    
                    Router.getUserContacts()
                    Router.getTransactionHistory(transId: "1.11.0")
                    
                    DispatchQueue.main.async {
                        DBManager.sharedInstance.updateUserData(name: nil, email: nil, imagePath: nil, currency: nil, bteBalance: tokenBalance, currencyBalance: balance)

                        let homeController = self.storyboard?.instantiateViewController(withIdentifier: "TabsViewController") as! TabsViewController
                        if let navController = self.navigationController {
                            navController.pushViewController(homeController, animated: true)
                        } else {
                            self.view.window?.rootViewController?.dismiss(animated: true, completion: {
                                NotificationCenter.default.post(name: NSNotification.Name("updateUserData"), object: nil)
                            })
                        }
                    }
                })
                ActivityIndicator.hide()
            }
        }
    }
    
    @IBAction func back(_ sender: UIButton) {
        if let navController = navigationController {
            navController.popViewController(animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func supportAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SupportViewController") as! SupportViewController
        self.navigationController?.present(myVC, animated: true, completion: nil)
    }
    
}

extension LoginViewController: FPNTextFieldDelegate {
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        return 
    }
    
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        countryCode = dialCode.replacingOccurrences(of: "+", with: "")
        let currency = (Locale.currency[code])!
        if let currencyCode = (currency as? (String, String))?.0 {
            labelMobilePrefix.text = currencyCode
        }
        
}
}
