//
//  ViewController.swift
//  Diamonium
//

import UIKit

class SplashScreenViewController: UIViewController {

    //MARK: IBOutlet
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var backView: UIView!
    
    var showCancel: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SinglePageViewController.indexDelegate = self
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        if showCancel {
            backView.isHidden = false
        } else {
            backView.isHidden = true
        }
    }
    
    //MARK: Action
    @IBAction func createWallet(_ sender: UIButton) {
        let createWalletController = self.storyboard?.instantiateViewController(withIdentifier: "CreateWalletViewController") as! CreateWalletViewController
        if let navController = self.navigationController {
            navController.pushViewController(createWalletController, animated: true)
        } else {
            self.puchController(controller: createWalletController)
        }
    }
    
    @IBAction func importWallet(_ sender: UIButton) {
        let loginController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        if let navController = self.navigationController {
            navController.pushViewController(loginController, animated: true)
        } else {
            self.puchController(controller: loginController)
        }
    }
    
    @IBAction func back(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
extension SplashScreenViewController: PageContentIndexDelegate {
    func getContentIndex(index: Int) {
        pageControl.currentPage = index
    }
}

