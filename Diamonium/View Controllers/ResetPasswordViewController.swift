//
//  ResetPasswordViewController.swift
//  Diamonium
//
//  Created by AM on 24/05/19.
//

import UIKit

class ResetPasswordViewController: UIViewController {
   
    
    @IBOutlet weak var textFieldConfirmPassword: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    
    var userName: String?
    var privateKey: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textFieldConfirmPassword.attributedPlaceholder = NSAttributedString(string:self.textFieldConfirmPassword.placeholder!, attributes:[NSAttributedString.Key.foregroundColor: UIColor.white])
        self.textFieldPassword.attributedPlaceholder = NSAttributedString(string:self.textFieldPassword.placeholder!, attributes:[NSAttributedString.Key.foregroundColor: UIColor.white])
    }
    
    @IBAction func showPassword(_ sender: UIButton) {
        if textFieldPassword.isSecureTextEntry {
            textFieldPassword.isSecureTextEntry = false
        } else {
            textFieldPassword.isSecureTextEntry = true
        }
    }
    
    @IBAction func showConfirmPassword(_ sender: UIButton) {
        if textFieldConfirmPassword.isSecureTextEntry {
            textFieldConfirmPassword.isSecureTextEntry = false
        } else {
            textFieldConfirmPassword.isSecureTextEntry = true
        }
    }
    
    @IBAction func restore(_ sender: UIButton) {
        if (textFieldPassword.text?.isEmpty)! {
            showAlert(title: "Required", message: "Enter Password", action1Name: "Ok", action2Name: nil)
        }else if (textFieldConfirmPassword.text?.isEmpty)! {
            showAlert(title: "Required", message: "Enter Confirm Password", action1Name: "Ok", action2Name: nil)
        } else if (textFieldPassword.text?.characters.count)! < 6 {
            showAlert(title: "Error", message: "Enter valid password", action1Name: "Ok", action2Name: nil)
        }else if (textFieldConfirmPassword.text != textFieldPassword.text){
            showAlert(title: "Error", message: "Enter valid confirm password", action1Name: "Ok", action2Name: nil)
        }else {
            ActivityIndicator.show(view: self.view)
           
            let param = [
                K_USERNAME: self.userName!,
                K_PRIVATE_KEY: self.privateKey!,
                K_PASSWORD: textFieldPassword.text!] as [String : Any]
            SessionManager.shared.methodForApiCalling(url: U_BASE2+U_UPDATE_PASSWORD, method: .post, parameter: param, objectClass: ResetResponse.self, requestCode: U_UPDATE_PASSWORD, userToken: nil) { response in
                self.showAlert(title: "Success", message:"Password Restored", action1Name: "Ok", action2Name: nil)
                let loginController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                self.navigationController?.pushViewController(loginController, animated: true)
                ActivityIndicator.hide()
            }
        }
    }

}
