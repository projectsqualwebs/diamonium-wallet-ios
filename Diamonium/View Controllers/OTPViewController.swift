//
//  OTPViewController.swift
//  Diamonium
//
//

import UIKit
import KWVerificationCodeView

protocol NumberVerified {
    func registerUser(phoneNumber:String)
}

class OTPViewController: UIViewController {

    //MARK: IBOutlet
    @IBOutlet weak var verificationCodeView: KWVerificationCodeView!
    @IBOutlet weak var resendCodeButton: UIButton!
    
    var otp: String?
    var phone = String()
    var countryCode = String()
    var counter = 60
    var numberDelegate: NumberVerified? = nil
        
    override func viewDidLoad() {
        super.viewDidLoad()
        verificationCodeView.textColor = .white
        verificationCodeView.underlineSelectedColor = .white
        verificationCodeView.delegate = self
        verificationCodeView.becomeFirstResponder()
         Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
    }
    
    @objc func updateCounter() {
           //example functionality
           if counter > 0 {
               counter -= 1
               self.resendCodeButton.setTitle("00:\(counter)", for: .normal)
               self.resendCodeButton.layoutIfNeeded()
               UIView.setAnimationsEnabled(false)
               self.resendCodeButton.isUserInteractionEnabled = false
           }else {
            self.resendCodeButton.isUserInteractionEnabled = true
            self.resendCodeButton.setTitle("Resend", for: .normal)
            UIView.setAnimationsEnabled(false)
           self.resendCodeButton.layoutIfNeeded()
           }
       }
    
    func apiCallToRegister() {
        ActivityIndicator.show(view: self.view)
        let param = [
            "mobile_number": self.countryCode + self.phone,
            "otp": self.otp
            ] as [String : Any]
        SessionManager.shared.methodForApiCalling(url: U_BASE2 + U_VERIFY_OTP, method: .post, parameter: param, objectClass: Register.self, requestCode: U_REGISTER, userToken: nil) { response in
            self.showAlert(title: nil, message: "Number verified", action1Name: "Ok", action2Name: nil)
            self.numberDelegate?.registerUser(phoneNumber: self.phone)
            self.navigationController?.popViewController(animated: true)
            ActivityIndicator.hide()
        }
    }
    
    //MARK: Action
    @IBAction func resendAction(_ sender: Any) {
           ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE2 + U_GENERATE_OTP, method: .post, parameter: ["mobile_number": ((self.countryCode ) + (self.phone ?? ""))], objectClass: Response.self, requestCode: U_GENERATE_OTP, userToken: nil) { (response) in
               ActivityIndicator.hide()
               self.counter = 60
            self.showAlert(title: nil, message: "An OTP is sent to your mobile number.", action1Name: "Ok", action2Name: nil)
             
           }
       }
    
    
    @IBAction func submit(_ sender: UIButton) {
        let code = verificationCodeView.getVerificationCode()
        if code == " " {
           showAlert(title: "Error", message: "Enter OTP", action1Name: "Ok", action2Name: nil)
        } else if (self.otp!.count < 6) {
            showAlert(title: "Incorrect OTP", message: "Enter correct OTP", action1Name: "Ok", action2Name: nil)
        } else {
         apiCallToRegister()
        }
    }
    
    @IBAction func back(_ sender: UIButton) {
        if let navController = navigationController {
            navController.popViewController(animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
}
extension OTPViewController: KWVerificationCodeViewDelegate {
    func didChangeVerificationCode() {
        if !(verificationCodeView.getVerificationCode().hasSuffix(" ")) {
            if (verificationCodeView.getVerificationCode().count == 6) {
                self.otp = verificationCodeView.getVerificationCode()
                
            } else {
                self.showAlert(title: "Incorrect OTP", message: "Enter correct OTP", action1Name: "Ok", action2Name: nil)
            }
        }
    }
}
